"""
Module for processing the sensitivities.
"""

import numpy as np

# METHODS TO CALCULATE SENSITIVITIES


def unit_sensitivity(**kwargs) -> float:
    """
    Return unit sensitivites (suppress sensitivities).

    Returns:
        float: Significance/sensitivity/weight.
    """
    return 1.0


def gaussian_approximation_wo_errors(**kwargs) -> float:
    """Calculate gaussian approximation
    of significance without errors.

    Args:
        kwargs (Dict): Keyword arguments.

    Returns:
        float: Significance/sensitivity/weight.
    """
    return kwargs["signal"] / np.sqrt(kwargs["background"])


def gaussian_approximation_w_errors(**kwargs) -> float:
    """Calculate gaussian approximation
    of significance with errors.

    Args:
        kwargs (Dict): Keyword arguments.

    Returns:
        float: Significance/sensitivity/weight.
    """
    return kwargs["signal"] / \
        np.sqrt(kwargs["background"] + kwargs["background_error"]**2)


def recommended_sensitivity_estimation(**kwargs) -> float:
    """Recommended sensitivity estimation from ATL-PHYS-PUB-2020-025.

    Args:
        kwargs (Dict): Keyword arguments.

    Returns:
        float: Significance/sensitivity/weight.
    """
    # variables to make the code more readable
    s = kwargs["signal"]
    b = kwargs["background"]
    n = s + b
    e = kwargs["background_error"]
    sensitivity: float = 0.0

    # check if e is zero
    if np.isclose(e, 0.0):
        if n >= b:
            sensitivity = np.sqrt(2 * (n * np.log(n / b) - (n - b)))
        else:
            sensitivity = -np.sqrt(2 * (n * np.log(n / b) - (n - b)))
    else:
        if n >= b:
            sensitivity = np.sqrt(2 * (n *
                                       np.log(n * (b + e**2) / (b**2 + n * e**2)) -
                                       (b**2 / e**2) *
                                       np.log(1 + (e**2 * (n - b)) / (b * (b + e**2)))))
        else:
            sensitivity = -np.sqrt(2 * (n *
                                        np.log(n * (b + e**2) / (b**2 + n * e**2)) -
                                        (b**2 / e**2) *
                                        np.log(1 + (e**2 * (n - b)) / (b * (b + e**2)))))
    return sensitivity


def recommended_sensitivity_estimation_wo_error(**kwargs) -> float:
    """Recommended sensitivity estimation from ATL-PHYS-PUB-2020-025.

    Args:
        kwargs (Dict): Keyword arguments.

    Returns:
        float: Significance/sensitivity/weight.
    """

    # variables to make the code more readable
    s = kwargs["signal"]
    b = kwargs["background"]
    n = s + b
    sensitivity: float = 0.0

    # check if s or b is zero
    if n >= b:
        sensitivity = np.sqrt(2 * (n * np.log(n / b) - (n - b)))
    else:
        sensitivity = -np.sqrt(2 * (n * np.log(n / b) - (n - b)))
    return sensitivity
