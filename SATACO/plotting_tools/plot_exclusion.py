"""
Plot exclusion plot for the results produced with the SATACO code algorithm.
"""


from typing import Any, Dict, List, Tuple, Union

import matplotlib.colors as mplc
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.lines import Line2D

from SATACO.plotting_tools.plot_data import label_map
from SATACO.plotting_tools.plot_style import CMAP, set_plot_style


def __apply_LSP_cut(df: pd.DataFrame,
                    LSP_type: str = "") -> pd.DataFrame:
    """
Apply LSP cut to dataframe.

Args:
    df (pd.DataFrame): Dataframe to apply LSP cut to.
    LSP_type (str, optional): Select particular LSP type, eg. 'bino'/'wino'/'higgsino'/'neutralino'. Defaults to "".

Returns:
    pd.DataFrame: Dataframe with LSP cut applied.
"""
# valid LSP types
    LSP_dict: Dict[str, List[float]] = {
        "all": [],
        "bino": [1.0],
        "wino": [2.0],
        "higgsino": [3.0],
        "neutralino": [1.0, 2.0, 3.0],
        "EWK": [1.0, 3.0],
        "stau": [1000015.0],
        "sneutrino": [1000012.0, 1000014.0, 1000016.0],
        "tau_sneutrino": [1000016.0],
        "mu_sneutrino": [1000014.0],
        "ele_sneutrino": [1000012.0]}
    known_LSP_types: List[str] = [t for t in LSP_dict]

 # check LSP_type is valid
    if LSP_type not in known_LSP_types:
        raise ValueError(
            f"LSP_type {LSP_type} not recognised. Possible types are {known_LSP_types}")

    LSP_nums: List[float] = LSP_dict[LSP_type]
    if len(LSP_nums) == 0:
        return df
    cut = df["LSP_type"] == LSP_nums[0]
    for n in LSP_nums[1:]:
        cut = (cut) | (df["LSP_type"] == n)
    df = df[cut]
    return df


def __apply_constraints(df: pd.DataFrame,
                        LSP_type: str = "",
                        verbose: bool = False) -> pd.DataFrame:
    """Apply contraints to dataframe.

    Args:
        df (pd.Dataframe): Dataframe to apply constraints to.
        LSP_type (str, optional): Select particular LSP type, eg. 'bino'/'wino'/'higgsino'/'neutralino'. Defaults to "".
        verbose (bool, optional): Print verbose output. Defaults to False.

    Returns:
        _type_: _description_
    """
    if LSP_type != "":
        n0 = len(df)
        df = __apply_LSP_cut(df, LSP_type)
        if verbose:
            print(len(df), "/", n0, "models have LSP type", LSP_type)
    return df


# custom colormap
def __make_custom_cmap() -> mplc.LinearSegmentedColormap:
    """
    Make custom colormap for the plots.

    Returns:
        mplc.LinearSegmentedColormap: Linear segmented colormap.
    """
    n = plt.cm.plasma.N
    cmaplist_magma: List[Any] = [plt.cm.magma(i) for i in range(n)]
    cmaplist: List[Any] = [plt.cm.plasma(i) for i in range(n)]
    cmaplist[0] = mplc.to_rgba("black")
    cmaplist[-1] = cmaplist_magma[-1]
    mycmap: mplc.LinearSegmentedColormap = mplc.LinearSegmentedColormap.from_list(
        "Custom cmap", cmaplist, n)
    mycmap.set_bad("gainsboro")
    return mycmap


def __set_axis_labels(ax: plt.Axes,
                      latex_dict: Dict,
                      xvar: str = "none",
                      yvar: str = "none",
                      fontsize: int = 14,
                      yloc: str = "top",
                      xloc: str = "right",
                      xlabelpad=None,
                      ylabelpad=None) -> None:
    """
    Set appropriate axis (ax) labels for each x and y variable

    ax: MatPlotLib axis object
    latex_dict: dictionary mapping from xvar and yvar to a latex label
    xvar, yvar: name of variables plotted on each axis
    fontsize: size of labels
    """
    if xvar != "none":
        if xvar in latex_dict:
            ax.set_xlabel(
                latex_dict[xvar], fontsize=fontsize, loc=xloc, labelpad=xlabelpad)
        else:
            ax.set_xlabel(xvar, fontsize=fontsize,
                          loc=xloc, labelpad=xlabelpad)
    if yvar != "none":
        if yvar in latex_dict:
            ax.set_ylabel(
                latex_dict[yvar], fontsize=fontsize, loc=yloc, labelpad=ylabelpad)
        else:
            ax.set_ylabel(yvar, fontsize=fontsize,
                          loc=yloc, labelpad=ylabelpad)
    return


def plot2Dbinned(df: pd.DataFrame,
                 xvar: str,
                 yvar: str,
                 save: str = "",
                 xlim: Tuple[float, float] = (1e+1, 1e+3),
                 ylim: Tuple[float, float] = (1e-7, 1e+1),
                 nxbins: int = 15,
                 nybins: int = 15,
                 figsize: Tuple[int, int] = (8, 6),
                 tickpad=None,
                 logx: bool = False,
                 logy: bool = False,
                 xedges=None,
                 yedges=None,
                 labelsize: int = 20,
                 dpi: int = 200,
                 nxticks=None,
                 nyticks=None,
                 z_func: str = "exclusion",
                 z_label: str = "Fraction of models excluded",
                 ann_func: str = "exclusion",
                 cNorm=None,
                 no_zero_ann: bool = True,
                 ann_size: int = 7,
                 LSP_type: str = "neutralino",
                 difference_values: pd.DataFrame = None,
                 cmap=__make_custom_cmap(),
                 textbox: bool = True,
                 textbox_x: float = 0.05,
                 textbox_y: float = 0.95,
                 setTitle: bool = True,
                 title: str = "") -> None:
    """
    Make 2D binned plot of CLs values from result dataframe in factory.

    Args:
        df (pd.DataFrame): Dataframe containing results.
        xvar (str): Name of variable to plot on x-axis.
        yvar (str): Name of variable to plot on y-axis.
        save (str, optional): Save plot to file. Defaults to "".
        xlim (Tuple[float, float], optional): x-axis limits. Defaults to (1e+1, 1e+3).
        ylim (Tuple[float, float], optional): y-axis limits. Defaults to (1e-7, 1e+1).
        nxbins (int, optional): Number of x-axis bins. Defaults to 15.
        nybins (int, optional): Number of y-axis bins. Defaults to 15.
        figsize (Tuple[int, int], optional): Figure size. Defaults to (8, 6).
        tickpad ([type], optional):  Defaults to None.
        logx (bool, optional): Log scale x-axis. Defaults to False.
        logy (bool, optional): Log scale y-axis. Defaults to False.
        xedges ([type], optional):  Defaults to None.
        yedges ([type], optional):  Defaults to None.
        labelsize (int, optional):  Defaults to 20.
        dpi (int, optional):  Defaults to 200.
        nxticks ([type], optional):  Defaults to None.
        nyticks ([type], optional):  Defaults to None.
        z_func (str, optional):  Defaults to "exclusion".
        z_label (str, optional):  Defaults to "Fraction of models excluded".
        ann_func (str, optional):  Defaults to "exclusion".
        cNorm ([type], optional):  Defaults to None.
        no_zero_ann (bool, optional):  Defaults to True.
        ann_size (int, optional):  Defaults to 7.
        LSP_type (str, optional):  Defaults to "neutralino".
        cmap ([type], optional):  Defaults to __make_custom_cmap().

    Returns:

    """
    if xvar == "m_chi_1pm":
        label_xvar = xvar
        xvar = "m_chi_1p"
    else:
        label_xvar = xvar

    if yvar == "m_chi_1pm":
        label_yvar = yvar
        yvar = "m_chi_1p"
    else:
        label_yvar = yvar
    # apply constraints
    df = __apply_constraints(df=df, LSP_type=LSP_type)

    fig, ax = plt.subplots(figsize=figsize)

    def _wrapped_funcs(_fString):

        def myFunction(x):
            if len(x) == 0:
                return float("nan")
            if _fString == "exclusion":
                return len(__apply_constraints(df=x,
                                               LSP_type=LSP_type)) / len(x)
            elif "mean_" in _fString:
                zvar = _fString.split("mean_")[1]
                return x[zvar].mean()
            elif "max_" in _fString:
                zvar = _fString.split("max_")[1]
                return x[zvar].max()
            elif "min_" in _fString:
                zvar = _fString.split("min_")[1]
                return x[zvar].min()
            elif "fraction_excluded_" in _fString:
                # for getting the fraction of models excluded, count the number of models with CLs < 0.01
                zvar = _fString.split("fraction_excluded_")[1]
                return len(x[x[zvar] < 0.01]) / len(x)
            elif "count_excluded_" in _fString:
                # for getting the fraction of models excluded, count the number of models with CLs < 0.01
                zvar = _fString.split("count_excluded_")[1]
                return len(x[x[zvar] < 0.01])
            elif _fString == "none" or _fString is None:
                return ""
            else:
                return 0
        return myFunction

    # bin dataframe and calculate value for each bin
    df = df.copy()
    if xedges is None:
        if logx:
            xedges = np.logspace(
                np.log10(xlim[0]), np.log10(xlim[1]), nxbins+1)
        else:
            xedges = np.linspace(xlim[0], xlim[1], nxbins+1)
    if yedges is None:
        if logy:
            yedges = np.logspace(
                np.log10(ylim[0]), np.log10(ylim[1]), nybins+1)
        else:
            yedges = np.linspace(ylim[0], ylim[1], nybins+1)
    xcut = pd.cut(df[xvar], xedges)
    ycut = pd.cut(df[yvar], yedges)

    df.loc[:, "xbins"] = pd.Categorical(xcut)
    df.loc[:, "ybins"] = pd.Categorical(ycut)
    df["xbins"] = pd.Categorical(df["xbins"])
    df["ybins"] = pd.Categorical(df["ybins"])

    lambdaFunc = _wrapped_funcs(z_func)

    F = df.groupby(["xbins", "ybins"]).apply(lambda x: lambdaFunc(x))

    F = F.reindex(pd.MultiIndex.from_product(
        [xcut.cat.categories, ycut.cat.categories])).unstack()
    xedgesFinal = [x.left for x in F.index]+[F.index[-1].right]
    yedgesFinal = [y.left for y in F.columns]+[F.columns[-1].right]

    # get values for annotation / circle sizing
    if ann_func is not None:
        annLambdaFunc = _wrapped_funcs(ann_func)
        annVals = df.groupby([xcut, ycut]).apply(lambda x: annLambdaFunc(x))
        annVals = annVals.reindex(pd.MultiIndex.from_product(
            [xcut.cat.categories, ycut.cat.categories])).unstack()

    if "count_excluded_" not in z_func:
        im = ax.pcolormesh(xedgesFinal, yedgesFinal, F.transpose(), shading="auto", vmin=0, vmax=max(
            1, int(np.nanmax(F.max(skipna=True)))), cmap=cmap, norm=cNorm)
    else:
        # cmap should be greens and 0 should be black
        custom_cmap = CMAP
        # custom_cmap.set_bad("white")
        custom_cmap.set_under("black")
        # make log scale colorbar and make 0 appear in black
        cNorm = mplc.LogNorm(vmin=1, vmax=max(
            1, int(np.nanmax(F.max(skipna=True)))))
        im = ax.pcolormesh(xedgesFinal, yedgesFinal, F.transpose(), shading="auto", cmap=custom_cmap,
                           norm=cNorm)
    if logx:
        ax.set_xscale("log")
    if logy:
        ax.set_yscale("log")

    # annotate plot
    if ann_func is not None:
        for x in annVals.index:
            for y in annVals.columns:
                annNum = np.nan_to_num(annVals.loc[x, y])
                if annNum == 0 and no_zero_ann == True:
                    continue
                if annNum >= 1:
                    annNum = int(annNum)
                    shift = 4
                elif annNum >= 0.1:
                    annNum = round(annNum, 2)
                    shift = 6
                elif annNum >= 0.001:
                    annNum = round(annNum, 3)
                    shift = 10
                else:
                    annNum = 0
                    shift = 4
                xLoc = x.left + (x.right-x.left)/shift
                yLoc = y.left + (y.right-y.left)/shift
                ann = ax.annotate(str(annNum), (xLoc, yLoc),
                                  fontsize=ann_size, color="white")
                ann.set_path_effects(
                    [PathEffects.withStroke(linewidth=2, foreground="k")])

    # other plotting things
    ax.set_xlim(left=xlim[0], right=xlim[1])
    if nxticks:
        ax.locator_params(axis="x", nbins=nxticks)
    if nyticks:
        ax.locator_params(axis="y", nbins=nyticks)

    if "count_excluded_" not in z_func:
        cbar = fig.colorbar(im)
    else:
        # make log scale colorbar and make 0 appear in black
        cNorm = mplc.LogNorm(vmin=1, vmax=max(
            1, int(np.nanmax(F.max(skipna=True)))))
        cbar = fig.colorbar(im, norm=cNorm)

    cbar.set_label(label=z_label, size=labelsize)
    cbar.ax.tick_params(labelsize=labelsize)

    ax.set_ylim(bottom=ylim[0], top=ylim[1])
    plt.xticks(fontsize=labelsize)
    plt.yticks(fontsize=labelsize)
    __set_axis_labels(ax=ax,
                      latex_dict=label_map,
                      xvar=label_xvar,
                      yvar=label_yvar,
                      fontsize=labelsize)

    # textbox
    if textbox:
        # add ATLAS label
        ax.text(textbox_x, textbox_y, r'\textbf{ATLAS} Internal'+'\n'+r'140 fb$^{-1}$ $\sqrt{s} =$ 13 TeV',
                transform=ax.transAxes, color='black', fontsize=11,
                verticalalignment='top', fontfamily='helvetica')

    if setTitle:
        ax.set_title(title, fontsize=labelsize, pad=8)

    if tickpad is not None:
        ax.tick_params(axis="both", which="major", pad=tickpad)
    plt.tight_layout()
    if save:
        fig.savefig(save, dpi=dpi)
        plt.close()
    else:
        plt.show()

    return


def plot2D(df: pd.DataFrame,
           xvar: str,
           yvar: str,
           cls_column_i: str,
           cls_column_j: str = None,
           comparison: str = "ratio",
           save: str = "",
           xlim: Tuple[float, float] = (1e+1, 1e+3),
           ylim: Tuple[float, float] = (1e-7, 1e+1),
           figsize: Tuple[int, int] = (8, 6),
           tickpad=None,
           logx: bool = False,
           logy: bool = False,
           labelsize: int = 20,
           dpi: int = 200,
           nxticks=None,
           nyticks=None,
           z_label: str = "Fraction of models excluded",
           color_log: bool = False,
           LSP_type: str = "neutralino",
           cmap=__make_custom_cmap(),
           textbox: bool = True,
           textbox_x: float = 0.05,
           textbox_y: float = 0.95,
           title: str = "") -> None:
    """
    Make 2D binned plot of CLs values from result dataframe in factory.

    Args:
        df (pd.DataFrame): Dataframe containing results.
        xvar (str): Name of variable to plot on x-axis.
        yvar (str): Name of variable to plot on y-axis.
        save (str, optional): Save plot to file. Defaults to "".
        xlim (Tuple[float, float], optional): x-axis limits. Defaults to (1e+1, 1e+3).
        ylim (Tuple[float, float], optional): y-axis limits. Defaults to (1e-7, 1e+1).
        nxbins (int, optional): Number of x-axis bins. Defaults to 15.
        nybins (int, optional): Number of y-axis bins. Defaults to 15.
        figsize (Tuple[int, int], optional): Figure size. Defaults to (8, 6).
        tickpad ([type], optional):  Defaults to None.
        logx (bool, optional): Log scale x-axis. Defaults to False.
        logy (bool, optional): Log scale y-axis. Defaults to False.
        xedges ([type], optional):  Defaults to None.
        yedges ([type], optional):  Defaults to None.
        labelsize (int, optional):  Defaults to 20.
        dpi (int, optional):  Defaults to 200.
        nxticks ([type], optional):  Defaults to None.
        nyticks ([type], optional):  Defaults to None.
        z_func (str, optional):  Defaults to "exclusion".
        z_label (str, optional):  Defaults to "Fraction of models excluded".
        ann_func (str, optional):  Defaults to "exclusion".
        cNorm ([type], optional):  Defaults to None.
        no_zero_ann (bool, optional):  Defaults to True.
        ann_size (int, optional):  Defaults to 7.
        LSP_type (str, optional):  Defaults to "neutralino".
        cmap ([type], optional):  Defaults to __make_custom_cmap().

    Returns:

    """
    if xvar == "m_chi_1pm":
        label_xvar = xvar
        xvar = "m_chi_1p"
    else:
        label_xvar = xvar

    if yvar == "m_chi_1pm":
        label_yvar = yvar
        yvar = "m_chi_1p"
    else:
        label_yvar = yvar
    # apply constraints
    df = __apply_constraints(df=df, LSP_type=LSP_type)

    fig, ax = plt.subplots(figsize=figsize)

    # bin dataframe and calculate value for each bin
    df = df.copy()

    # only plot points that have a value
    df = df[df[cls_column_i] >= 0]

    # scatter plot of all points that have value
    if cls_column_j is not None:
        if comparison == "ratio":
            ratio = df[cls_column_i] / df[cls_column_j]
            im = ax.scatter(df[xvar], df[yvar], c=ratio,
                            norm=mplc.LogNorm(vmin=1e-2, vmax=1e+2),
                            cmap="coolwarm", s=20, alpha=0.5)

        elif comparison == "diff":
            diff = df[cls_column_i] - df[cls_column_j]
            im = ax.scatter(df[xvar], df[yvar], c=diff,
                            cmap="coolwarm", s=20, alpha=0.5)

        else:
            raise ValueError(
                f"Comparison {comparison} not recognised. Possible options are 'ratio' or 'diff'.")
    else:
        if color_log:
            im = ax.scatter(df[xvar], df[yvar], c=df[cls_column_i],
                            norm=mplc.LogNorm(), cmap=cmap, s=20, alpha=0.5)
        else:
            im = ax.scatter(df[xvar], df[yvar], c=df[cls_column_i],
                            cmap=cmap, s=20, alpha=0.5)

    if logx:
        ax.set_xscale("log")
    if logy:
        ax.set_yscale("log")

    # other plotting things
    ax.set_xlim(left=xlim[0], right=xlim[1])
    if nxticks:
        ax.locator_params(axis="x", nbins=nxticks)
    if nyticks:
        ax.locator_params(axis="y", nbins=nyticks)

    cbar = fig.colorbar(im)
    cbar.set_label(label=z_label, size=labelsize)
    cbar.ax.tick_params(labelsize=labelsize)

    ax.set_ylim(bottom=ylim[0], top=ylim[1])
    plt.xticks(fontsize=labelsize)
    plt.yticks(fontsize=labelsize)
    __set_axis_labels(ax=ax,
                      latex_dict=label_map,
                      xvar=label_xvar,
                      yvar=label_yvar,
                      fontsize=labelsize)
    if tickpad is not None:
        ax.tick_params(axis="both", which="major", pad=tickpad)

    # textbox
    if textbox:
        # add ATLAS label
        ax.text(textbox_x, textbox_y, r'\textbf{ATLAS} Internal'+'\n'+r'140 fb$^{-1}$ $\sqrt{s} =$ 13 TeV',
                transform=ax.transAxes, color='black', fontsize=11,
                verticalalignment='top', fontfamily='helvetica')

    if title:
        ax.set_title(title, fontsize=labelsize, pad=8)

    plt.tight_layout()
    if save:
        fig.savefig(save, dpi=dpi)
        plt.close()
    else:
        plt.show()

    return


def scatter_exclusion(df: pd.DataFrame,
                      xvar: str,
                      yvar: str,
                      logx: bool = True,
                      logy: bool = True,
                      diagonal: bool = True,
                      save: str = "",
                      lines: Union[List[float], None] = None,
                      xlim: Tuple[float, float] = (0., 1.),
                      ylim: Tuple[float, float] = (0., 1.),
                      dpi: int = 200,
                      textbox: str = True,
                      textbox_x: float = 0.05,
                      textbox_y: float = 0.95
                      ) -> None:
    """
    Scatter plot of CLs values from result dataframe in factory.

    Args:
        df (pd.DataFrame): Dataframe containing results.
        xvar (str): Name of variable to plot on x-axis.
        yvar (str): Name of variable to plot on y-axis.
        diagonal (bool, optional): Plot diagonal line. Defaults to True.
        save (str, optional): Save plot to file. Defaults to "".
        logx (bool, optional): Log scale x-axis. Defaults to False.
        logy (bool, optional): Log scale y-axis. Defaults to False.
        xlim (Tuple[float, float], optional): x-axis limits. Defaults to (0., 1.). (Used for CLs values)
        ylim (Tuple[float, float], optional): y-axis limits. Defaults to (0., 1.). (Used for CLs values)
        dpi (int, optional):  Defaults to 200.
        textbox (str, optional):  ATLAS label. Defaults to True.
        textbox_x (float, optional):  Defaults to 0.05.
        textbox_y (float, optional):  Defaults to 0.95.

    Returns:
        None
    """
    if xvar == "m_chi_1pm":
        label_xvar = xvar
        xvar = "m_chi_1p"
    else:
        label_xvar = xvar

    if yvar == "m_chi_1pm":
        label_yvar = yvar
        yvar = "m_chi_1p"
    else:
        label_yvar = yvar
    # only plot points that have a value
    df = df[df[xvar] >= 0]

    # set ATLAS style
    set_plot_style(matrix=False)

    # make figure
    fig, ax = plt.subplots(figsize=(8, 6))

    # make scatter plot
    ax.scatter(df[xvar], df[yvar], s=10, alpha=0.5, label="CLs")

    if diagonal:
        plt.plot([1e-10, 1], [1e-10, 1], color="black", linestyle="--",
                 label="CLs SATACO = CLs Regular")

    if lines is not None:
        # vertical and horizontal lines in red
        for line in lines:
            plt.axvline(x=line, color="red", linestyle="-")
            plt.axhline(y=line, color="red", linestyle="-")

    # set axis scales
    if logx:
        ax.set_xscale("log")
        if xlim[0] == 0:
            xlim = (1e-10, xlim[1])
    if logy:
        ax.set_yscale("log")
        if ylim[0] == 0:
            ylim = (1e-10, ylim[1])

    # set axis limits
    ax.set_xlim(left=xlim[0], right=xlim[1])
    ax.set_ylim(bottom=ylim[0], top=ylim[1])

    # set axis labels
    __set_axis_labels(ax=ax,
                      latex_dict=label_map,
                      xvar=label_xvar,
                      yvar=label_yvar)

    # set legend
    ax.legend(loc="center left", handles=[
        Line2D([0], [0], color="blue", linestyle="",
               marker="o", markersize=3, label="CLs", alpha=0.5),
        Line2D([0], [0], color="black", linestyle="--",
               label="CLs SATACO = CLs Regular"),
        Line2D([0], [0], color="red", linestyle="-", label="CLs categories")])

    # textbox
    if textbox:
        # add ATLAS label
        text_box = ax.text(textbox_x, textbox_y, r'\textbf{ATLAS} Internal'+'\n'+r'140 fb$^{-1}$ $\sqrt{s} =$ 13 TeV',
                           transform=ax.transAxes, color='black', fontsize=11,
                           verticalalignment='top', fontfamily='helvetica')

    # save figure
    if save:
        fig.savefig(save, dpi=dpi)
        plt.close()
    else:
        plt.show()

    return


def plot2Dhist(df: pd.DataFrame,
               xvar: str,
               yvar: str,
               save: str = "",
               atlas_label='Number of models',
               xlim: Tuple[float, float] = (1e+1, 1e+3),
               ylim: Tuple[float, float] = (1e-7, 1e+1),
               bins: int = 15,
               logx: bool = False,
               logy: bool = False,
               label_size: int = 12,
               figsize: Tuple[int, int] = (8, 6),
               tickpad=None,
               dpi=200,
               ann_size: int = 7,
               cmap=CMAP):
    """_summary_

    Args:
        df (pd.DataFrame): Dataframe containing results.
        xvar (str): Name of variable to plot on x-axis.
        yvar (str): Name of variable to plot on y-axis.
        save (str, optional): Save plot to file. Defaults to "".
        xlim (Tuple[float, float], optional): Limits of x-axis. Defaults to (1e+1, 1e+3).
        ylim (Tuple[float, float], optional): Limits of y-axis. Defaults to (1e-7, 1e+1).
        bins (int, optional): Number of bins. Defaults to 15.
        logx (bool, optional): Log scale x-axis. Defaults to False.
        logy (bool, optional): Log scale y-axis. Defaults to False.
        label_size (int, optional): Size of labels. Defaults to 12.
        figsize (Tuple[int, int], optional): Size of figure. Defaults to (8, 6).
        dpi (int, optional): Defaults to 200.
        ann_size (int, optional): Size of annotation. Defaults to 7.
        cmap (_type_, optional): Colormap. Defaults to CMAP.
    """
    if xvar == "m_chi_1pm":
        label_xvar = xvar
        xvar = "m_chi_1p"
    else:
        label_xvar = xvar

    if yvar == "m_chi_1pm":
        label_yvar = yvar
        yvar = "m_chi_1p"
    else:
        label_yvar = yvar

    fig, ax = plt.subplots(figsize=figsize)

    if cmap:
        hist_cmap = cmap
    else:
        hist_cmap = plt.cm.plasma
        hist_cmap.set_bad('gainsboro')

    hist = ax.hist2d(df[xvar], df[yvar], bins=bins,
                     range=[[xlim[0], xlim[1]], [ylim[0], ylim[1]]],
                     cmap=hist_cmap, norm=mplc.LogNorm())
    try:
        cbar = fig.colorbar(hist[3])
    except ValueError:
        print('WARNING: Hist2D empty for x = ', xvar,
              ', y = ', yvar, ' -- Skipping plot')
        return
    if logx == True:
        ax.set_xscale('log')
    if logy == True:
        ax.set_yscale('log')

    cbar.set_label(label='Number of models', size=label_size)
    cbar.ax.tick_params(labelsize=label_size)

    plt.xticks(fontsize=label_size)
    plt.yticks(fontsize=label_size)
    ax.set_ylim(ylim)
    ax.set_xlim(xlim)

    # Annotation on each bin with non-zero value the number of models in that bin
    shift: int = 4
    for i in range(bins):
        for j in range(bins):
            if hist[0][i, j] > 0:
                if i == bins-1:
                    # if i == bins-1, then i+1 is out of range
                    xLoc = hist[1][i] + (hist[1][i]-hist[1][i-1])/shift
                else:
                    xLoc = hist[1][i] + (hist[1][i+1]-hist[1][i])/shift
                if j == bins-1:
                    # if j == bins-1, then j+1 is out of range
                    yLoc = hist[2][j] + (hist[2][j]-hist[2][j-1])/shift
                else:
                    yLoc = hist[2][j] + (hist[2][j+1]-hist[2][j])/shift

                ann = ax.annotate(str(int(hist[0][i, j])),
                                  (xLoc, yLoc),
                                  fontsize=ann_size,
                                  color="white")
                ann.set_path_effects(
                    [PathEffects.withStroke(linewidth=2, foreground="k")])

    __set_axis_labels(ax=ax,
                      latex_dict=label_map,
                      xvar=label_xvar,
                      yvar=label_yvar)

    if tickpad is not None:
        ax.tick_params(axis="both", which="major", pad=tickpad)

    # add ATLAS label
    ax.text(0.05, 0.95, r'\textbf{ATLAS} Internal'+'\n'+r'140 fb$^{-1}$ $\sqrt{s} =$ 13 TeV',
            transform=ax.transAxes, color='black', fontsize=11,
            verticalalignment='top', fontfamily='helvetica')

    # draw a box around the text

    if save:
        fig.savefig(save, dpi=dpi)
        plt.close()
    else:
        plt.show()

    return
