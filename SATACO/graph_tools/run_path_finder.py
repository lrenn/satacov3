"""
Running the path finder algorithm and returning the longest sensitive path.
"""

from typing import Any, Dict, List

import numpy as np

from SATACO.graph_tools.path_finder import PathFinder
from SATACO.graph_tools.sensitivity_combination import stouffers_method
from SATACO.graph_tools.transform_to_readable import \
    __indices_to_sr_names as indices_to_sr_names


def run_path_finder(
    correlation_matrix: np.ndarray,
        top_combinations: int,
        correlation_threshold: float,
        column_names: List[str],
        sensitivity_dict: Dict[str, float],
        combination_method: str = 'stouffer') -> Dict[int, Dict[str, Any]]:
    """
    Running the path finder algorithm and returning the longest sensitive path.

    Args:
        correlation_matrix (np.ndarray): Correlation matrix of the
            signal regions.
        top_combinations (int): Number of top combinations to return.
        correlation_threshold (float): Threshold for the correlation matrix.
        column_names (List[str]): Names of the signal regions.
        sensitivity_dict (Dict[str, float]): Dictionary of the sensitivities
            of the signal regions.
        combination_method (str, optional): Method to combine the
            sensitivities. Defaults to 'stouffer'. Also possible: 'adding'.

    Returns:
        Dict[int, Dict[str, Any]]: Dictionary of the top combinations.
    """

    # initialize path finder
    path_finder: PathFinder = PathFinder(
        correlations=correlation_matrix,
        threshold=correlation_threshold,
        weights=list(sensitivity_dict.values()))

    # correct method combining the sensitivities
    if combination_method == 'stouffer':
        path_finder.set_weight_func(
            lambda path: stouffers_method(path=path,
                                          weight=path_finder.weights))
    elif combination_method == 'adding':
        pass  # default method

    # run path finder
    highest_sensitivity_paths: Dict[int, Dict[str, Any]
                                    ] = path_finder.find_path(
                                        top=top_combinations)

    # transform the indices to the corresponding signal region names
    highest_sensitivity_paths = indices_to_sr_names(
        sr_names=column_names,
        result_dict=highest_sensitivity_paths)

    return highest_sensitivity_paths
