# SATACO

For more information about the project, please refer to the thesis (to be uploaded).

## Description

The package contains all tools for combining different analyses. This package is used within the [pMSSMFactory](https://gitlab.cern.ch/atlas-phys-susy-wg/summaries/pmssm/pMSSMFactory).

## Installlation

 To use the package, you need to install it first. The package is based on the gitlab of cern and can therefore be installed by using the following command:

```bash
pip install git+https://gitlab.cern.ch/lrenn/satacov3.git
```

If you want to work on the package, you can clone it first and afterwards work on it locally:

```bash
git clone https://gitlab.cern.ch/lrenn/satacov3.git
cd satacov3
```

Make sure that a python version of at least **3.7.6** is installed on your system. You can check this by using the following command:

```bash
python --version
```

Make a virtual environment if you want to work on the package and activate it with the following commands:

```bash
cd ..
python -m venv sataco-venv
source sataco-venv/bin/activate
```

After activating the virtual environment, you can install all necessary packages:

```bash
cd sataco
pip install -r requirements.txt
```

## Usage

As stated above the code is written for a specific framework and therefore the usage is limited to this framework. However, If you find code that is useful for you, feel free to use and modify it. (Don't forget to give credit to the authors of the code.)

## Packages used in SATACO

matplotlib<br>
numpy<br>
pandas<br>
networkx<br>
scipy<br>
pytest<br>
more-itertools<br>
scikit-optimize<br>
wheel<br>
mplhep<br>
uproot<br>

## Additional

For more information on how the package is used within the pMSSMFactory, please refer to the docs/info/INFO.md file.



