"""
Script to install SATACO package.
"""

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()


setuptools.setup(
    name="SATACO",
    version="0.1.47",
    author="Leon Renn",
    author_email="leon.renn@cern.ch",
    description="Automated combination of SUSY searches",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.cern.ch/lrenn/satacov2",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    license='MIT',
    python_requires='>=3.7.6',
    install_requires=["matplotlib>=3.5.3",
                      "numpy>=1.21.6",
                      "pandas>=1.3.5",
                      "networkx>=2.6",
                      "scipy>=1.7.0",
                      "pytest>=7.2.2",
                      "more-itertools>=9.0.0",
                      "scikit-optimize>=0.9.0",
                      "mplhep>=0.3.31",
                      "uproot>=5.1.2"
                      ],
)
