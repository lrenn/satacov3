# SATACO in pMSSM Factory

Information on the usage of the SATACO package in the [pMSSMFactory](https://gitlab.cern.ch/atlas-phys-susy-wg/summaries/pmssm/pMSSMFactory).

## Schematic Integration of the SATACO Project

![](sataco_workflow.png)

The **<font color="#0096FF">blue</font>**  boxes are the steps that are the general steps of the pMSSM Factory. The **<font color="orange">orange</font>** boxes are the steps that are required for the combination algorithm. The **<font color="lightgreen">green</font>** and **<font color="pink">pink</font>** boxes are the steps that are required for the learning algorithm.<br>

## Installation

The initialisiation script of the pMSSMFactory  <span style="color: #0096FF; text-decoration: underline;">init.sh</span>
 will source the  <span style="color: #0096FF; text-decoration: underline;">pMSSMFactory/setup\_sataco.sh</span> script.<br>

It is crucial that the  <span style="color: #0096FF; text-decoration: underline;">pMSSMFactory/setup\_sataco.sh</span> script is sourced successfully, otherwise the SATACO package and the corresponding scripts will not work.<br>

The ModelGeneration package is automatically setup via the <span style="color: #0096FF; text-decoration: underline;">pMSSMFactory/setup\_ModelGeneration.sh</span> script. However, this should be done manually before using the learning loop.<br>

## Explanation of Description

All the commands described here are structured in the following way:

```bash
command [-optional_specifier/--optional_specifier_long <arguments_optional_specifier>]  -specifier/--specifier_long <arguments_specifier> <argument_wo_specifier>
```

## Workflow

In the square brackets are optional specifiers that have default arguments implemented in the code. Necessary, specifiers follow after the square brackets. Sometimes, arguments need to be given that are not assigned to a specifier. 

### Combination of Analyses (with loaded matrix)

In order to work with the combination algorithm, the following steps are needed:<br>

1. Load the global matrix that is stored in the <span style="color: #0096FF; text-decoration: underline;">pMSSMFactory/Analyses/OverlapMatrix/\<ProdType\></span>. In the scope of the project, the matrix for the EWK sector of the pMSSM is generated and stored. Loading a matrix is done by executing the following command:

    ```bash
    (bash) $ generateOverlapMatrix.py [-cs/--config_section <config_section>] 
    ```

    This generates the  <span style="color: #0096FF; text-decoration: underline;">pMSSMFactory/SATACO/matrices/overlaps/overlap\_matrix.txt</span> file and different statistic matrices that are required for further processing.<br>

    The configs for this process are specified in the  <span style="color: #0096FF; text-decoration: underline;">pMSSMFactory/SATACO/configs/sataco.config</span> file.<br>

2. This part is **optional but recommened**. The application of the CR filter removes allowed combination of SRs that are controlled by the same or overlapping controll regions and make them therefore also overlapping. This is done by executing the following command:

    ```bash
    (bash) $ applyCRFilter.py
    ```

    The overlap matrix is changed accordingly and the new matrix is stored in the  <span style="color: #0096FF; text-decoration: underline;">pMSSMFactory/SATACO/matrices/overlaps/overlap\_matrix.txt</span> file. This step also impacts the upper limits matrix and the summary statistics matrix.

    The old matrixces can be easily obtained by rerunning step 1.<br>

3. After this optional step one can run **\<ProdType\>SatacoCombination** in the typical way of the pMSSMFactory for every model point.

    ```bash
    (bash) $ process.py -s <ProdType>SatacoCombination <models>
    ```

4. To consider the combinations in the fitting step, the **[-t]** flag needs to be raised when executing the **\<ProdType\>Fit** step.

    ```bash
    (bash) $ process.py -t -s <ProdType>TruthFitter <models>
    ```

### Combination of Analyses (with generated matrix)

If you want to generate a new matrix of shared events from scratch, the following steps are needed:<br>

1. When executing the **\<ProdType\>TruthAcceptance** step you have to add the option **\[-o\]** (For statistical stability, it is recommended to use lots of data). This will generate shared event matrices on the eos.

    ```bash
    (bash) $ process.py -o -s <ProdType>TruthAcceptance <models>
    ```

2. To combine the shared event matrices and copy them your local factory, you have to execute the following command:
    
    ```bash
    (bash) $ copySharedEventsMatrix.py
    ```

    This will copy the shared event matrices from the eos to the  <span style="color: #0096FF; text-decoration: underline;">pMSSMFactory/SATACO/matrices/shared\_events</span> directory and timestamp them.<br>
   
3. To generate the overlap matrix from scratch, you have to execute the following command:

    ```bash
    (bash) $ generateOverlapMatrix.py [-cs/--config_section DEFAULT] -o
    ```

    The -o flag is needed to tell the script that a new overlap matrix should be generated. The new matrix is stored in the  <span style="color: #0096FF; text-decoration: underline;">pMSSMFactory/SATACO/matrices/overlaps/overlap\_matrix.txt</span> file.<br>

    You can still go with the global matrix by leaving out the -o flag.<br>

4. After this you can follow the steps 2, 3 and 4 from the previous section.

### Learning Algorithm

The learning algorithm is used to actively learn the next points that are requested for the event generation that will populate a certain combination.<br>

To start with the learning algorithm, you have to have generated the overlap matrix either from scratch or loaded.<br>

Before running the algorithm, check that your configuration file is valid.<br>

1. The next two steps are two commands can be executed in parallel but do similar things. One of them is putting all parameters of each model point into a csv file.
    ```bash
    (bash) $ pMSSMParamsToCSV.py
    ```
    The other one is putting all acceptances of each model point into a csv file.
    ```bash
    (bash) $ acceptancesToCSV.py
    ```
    Both files are stored in the  <span style="color: #0096FF; text-decoration: underline;">pMSSMFactory/SATACO/pmssm</span> directory.<br>

2. Afterwards, you can run the command to propose combinations that could be learned to improve the overlap matrix.<br>

    ```bash
    (bash) $ proposeLearnableOverlapCombinations.py [--save] 
    ```

    If the save flag is not raised, the proposed combinations are only printed to the command line. If the save flag is raised, the proposed combinations are stored in the  <span style="color: #0096FF; text-decoration: underline;">pMSSMFactory/SATACO/matrices/statistics/proposed_combinations.txt</span> file.<br>

    Proposing combinations means that the algorithm looks at all matrix entries of the sufficient statistics matrix that are equal to -1 and proposes combinations that are learnable. Additionally, it cross checks if the combination is learnable by looking at the csv files that were generated one step before. If both regions accepted at least on event, the combination is considered to be learnable.<br>

    You can just copy the values that are printed on the command line and inject them into the learning algorithm later on.<br>

3. The next step is to actively learn the next points that are requested for the event generation that will populate a certain combination.<br>

    ```bash
    (bash) $ activelyLearnOverlaps.py [-cs/--config_section <config_section>] -comb/--combination <SR_i&SR_j>
    ```

    This is a batch job and will take some time. The outputs of this are stored in the  <span style="color: #0096FF; text-decoration: underline;">pMSSMFactory/SATACO/pmssm/pmssm_next_points.csv</span> file.<br>

4. The model generation from these points is done with the following command:
    ```bash
    (bash) $ generateALModels.py -comb/--combination <SR_i&SR_j>
    ```
    This is a batch job and will take some time. It is not yet fixed how the outputs will be reinjected into the factory. This is still work in progress.<br>

## Commands 

### Part of the Combination Workflow

| Command | Options | Description |
| --- | --- | --- |
| <ProdType>TruthAcceptance | [-o] | Generates the shared event matrices for the combination algorithm on the eos or if the matrix is not generated from scratch, it will just calculate the acceptances. |
| copySharedEventsMatrix.py |  | Adds the shared event matrices from all model points together and copies them to the local factory folder. |
| generateOverlapMatrix.py | [-cs/--config_section <config_section>] [-o] | Generates the overlap matrix from the shared event matrices, if the matrix needs to be generated from a new scan the -o flag needs to be raised. |
| applyCRFilter.py |  | Applies the CR filter to the overlap matrix. |
| <ProdType\>SatacoCombination |  | Runs the actual combination algorithm for each model point on the bacth system. |


### Part of the Learning Procedure

| Command | Options | Description |
| --- | --- | --- |
| pMSSMParamsToCSV.py |  | Prints all the pMSSM parameters of each model point into a csv file. |
| acceptancesToCSV.py |  | Prints all the acceptances of each model point into a csv file. |
| proposeLearnableOverlapCombinations.py | [--save] | Proposes combinations that could be learned to improve the overlap matrix. |
| activelyLearnOverlaps.py | [-cs/--config_section <config_section>] -comb/--combination <SR_i&SR_j> | Actively learns the next points that are requested for the event generation that will populate a certain combination. |
| generateALModels.py | -comb/--combination <SR_i&SR_j> | Starts the model generation for the actively learned points. |

### Plotting

| Command | Options | Description |
| --- | --- | --- |
| plotSatacoMatrices.py | [-i/--index_i <starting_index> <ending_index>] [-llh/--likelihood_scans] [-nl/--no_labels] [-gp/--group_production_mode] | Plots the overlap matrix and the upper limits matrix. |
| plotLowEfficiencyDistribution.py | [-t/--threshold <threshold>] | Plots the distribution of pMSSM parameters from the parameter csv file that is generated by the **pMSSMParamsToCSV.py** for a certain threshold of accepted events rate per model point for all regions available in the pMSSMFactory. |
| plotAcceptedEvents.py | [-f/--focus <analysis_name>] | Plots the accepted events per SR for each Analysis in a bar plot. |
| plotSatacoExclusion.py | [--subset <subset>] [--CLsCalc <type_of_summary>] [--decisionType <decide_on_BestExp_or_BestObs>] [--susyParticles <susy_particle_x,susy_particle_y>] | Plot the binned CLs values for the combination algorithm (exp and obs) for certain susy particles in the theory. |
| plotSatacoTransitionMatrix.py | [--subset <subset>] [--bin_number <bin_number>] | Plot the transition matrix for model points that change the exclusion status by the combination algorithm. |
| plotSatacoPathAnalysis.py | [--subset <subset>] [--bin_number <bin_number>] | Plot the distribution of path lengths for all model points. |
| plotSatacoMatrixEntries.py | [-cs/--config_section <config_section>] -i/--indices_i <indices_i_cs> -j/--indices_j <indices_j_cs> --amount_of_models <random_subsample> | Plot the distribution of overlap values for specific combinations of SRs for all model points. |

### Other

| Command | Options | Description |
| --- | --- | --- |
| satacoConfigs.py |  | Prints all the config values for the combination algorithm. |
| learningConfigs.py |  | Prints all the config values for the learning algorithm. |
| satacoFittingFiles.py | [-s/--save <name>] -m/--model <model> / -p/--path <orthogonalRegions> | Saves or prints the fitting files for a certain model point or a certain combination of regions. |
| listingSRandMetaData.py | --tex --text | Lists all analyses and regions in a tex or text file. |

