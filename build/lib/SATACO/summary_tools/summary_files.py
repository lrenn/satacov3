"""
Tools for writing summary files and saving them to destination folders.
"""

import os
from datetime import datetime
import numpy as np
from SATACO.summary_tools.summary_plots import summary_plots
from SATACO.summary_tools.summary_timestemp import append_timestamp_to_filename
from SATACO.config_tools.read_sataco_configs import read_sataco_configs
from typing import List, Dict


def writing_summary_of_run(cmd_arg: str,
                           classified_paths: Dict[str, List[str]],
                           filename: str = "",
                           destination_folder: str = "",
                           path_config_file: str = "",
                           ) -> None:
    """
    Write a summary of the run to a file.

    Args:
        filename (str): The name of the file to write to.
        destination_folder (str): The destination folder to save the file to.
        config_file (str): The path to the config file.

    Returns:
        None
    """
    if filename == "":
        filename = "summary_of_run.txt"
    filename = append_timestamp_to_filename(filename)

    config_values = read_sataco_configs(path_config_file=path_config_file)

    with open(os.path.join(destination_folder, filename), "w") as f:
        f.write("Summary of the run\n")
        f.write("------------------\n")
        f.write("Timestamp: {}\n".format(
            datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
        f.write("------------------\n")
        f.write(f"Correlation matrix: {config_values['correlation_matrix']}\n")
        f.write(f"Weight method: {config_values['weight_method']}\n")
        f.write(f"Correlation method: {config_values['correlation_method']}\n")
        f.write(f"Stats: {config_values['stats']}\n")
        f.write(f"Confidence level: {config_values['confidence_level']}\n")
        f.write(
            f"Correlation threshold: {config_values['correlation_threshold']}\n")
        f.write(f"Combination method: {config_values['combination_method']}\n")
        f.write(f"Combination: {config_values['combination']}\n")
        f.write(f"Top combinations: {config_values['top_combinations']}\n")
        f.write("------------------\n")
        # print all event_paths files
        f.write(
            f"Event files {len(classified_paths['event_paths'])} with given argument {cmd_arg}:\n")
        for event_path in classified_paths['event_paths']:
            f.write(f"{event_path}\n")
        f.write("------------------\n")

    return


def save_and_plot(overlap_matrix: np.ndarray,
                  significant_correlations: np.ndarray,
                  greater_pvalues: np.ndarray,
                  shared_events: np.ndarray,
                  accepted_events_either: np.ndarray,
                  data_destination_folder: str = "",
                  column_names: List[str] = [],
                  save_overlap_matrix: str = "",
                  save_statistics_matrices: str = "",
                  save_events_matrices: str = "",
                  plot: bool = True) -> None:
    """
    Save the overlap matrix, the statistics matrices and the events matrices.
    Also plot them if plot is True.

    Args:
        overlap_matrix (np.ndarray): The overlap matrix.
        significant_correlations (np.ndarray): The matrix of significant correlations.
        greater_pvalues (np.ndarray): The matrix of p-values.
        shared_events (np.ndarray): The matrix of shared events.
        accepted_events_either (np.ndarray): The matrix of accepted events.
        column_names (List[str]): The names of the columns.
        save_overlap_matrix (str): The path to save the overlap matrix.
        save_statistics_matrices (str): The path to save the statistics matrices.
        save_events_matrices (str): The path to save the events matrices.
        plot (bool): Whether to plot the matrices.

    Returns:
        None
    """
    # saving the data to the destination folder
    np.save(append_timestamp_to_filename(
        f"{data_destination_folder}/local_correlation_matrix.npy"), overlap_matrix)
    np.save(append_timestamp_to_filename(
        f"{data_destination_folder}/significant_correlations.npy"), significant_correlations)
    np.save(append_timestamp_to_filename(
        f"{data_destination_folder}/p_values_greater.npy"), greater_pvalues)
    np.save(append_timestamp_to_filename(
        f"{data_destination_folder}/shared_events.npy"), shared_events)
    np.save(append_timestamp_to_filename(
        f"{data_destination_folder}/accepted_events_either.npy"), accepted_events_either)

    # plotting the data
    if plot:
        summary_plots(overlap_matrix=overlap_matrix,
                      significant_correlations=significant_correlations,
                      greater_pvalues=greater_pvalues,
                      shared_events=shared_events,
                      accepted_events_either=accepted_events_either,
                      column_names=column_names,
                      save_overlap_matrix=save_overlap_matrix,
                      save_statistics_matrices=save_statistics_matrices,
                      save_events_matrices=save_events_matrices)

    return
