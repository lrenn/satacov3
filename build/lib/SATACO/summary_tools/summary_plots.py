from SATACO.plotting_tools.plot_matrices import (
    plot_overlap_matrix, plot_statistics_matrices, plot_events_matrices)
from SATACO.summary_tools.summary_timestemp import append_timestamp_to_filename

import numpy as np
from typing import List


def summary_plots(overlap_matrix: np.ndarray,
                  significant_correlations: np.ndarray,
                  greater_pvalues: np.ndarray,
                  shared_events: np.ndarray,
                  accepted_events_either: np.ndarray,
                  column_names: List[str] = [],
                  save_overlap_matrix: str = "",
                  save_statistics_matrices: str = "",
                  save_events_matrices: str = "") -> None:
    """
    Plot the overlap matrix, the statistics matrices and the events matrices.

    Args:
        overlap_matrix: the overlap matrix
        significant_correlations: the matrix of significant correlations
        greater_pvalues: the matrix of p-values
        shared_events: the matrix of shared events
        accepted_events_either: the matrix of accepted events
        column_names: the names of the columns
        save_overlap_matrix: the path to save the overlap matrix
        save_statistics_matrices: the path to save the statistics matrices
        save_events_matrices: the path to save the events matrices

    Returns:
        None
    """
    # if no column names are provided, use numbers
    if column_names == []:
        column_names = [str(i) for i in range(overlap_matrix.shape[0])]
    # plot (and save) overlap matrix
    plot_overlap_matrix(matrix=overlap_matrix,
                        column_names=column_names,
                        save=append_timestamp_to_filename(save_overlap_matrix))

    # plot (and save) statistics matrices
    plot_statistics_matrices(significant_correlations=significant_correlations,
                             greater_pvalues=greater_pvalues,
                             column_names=column_names,
                             save=append_timestamp_to_filename(save_statistics_matrices))

    # plot (and save) events matrices
    plot_events_matrices(shared_events=shared_events,
                         accepted_events_either=accepted_events_either,
                         column_names=column_names,
                         save=append_timestamp_to_filename(save_events_matrices))

    return
