"""
Methods for combination of sensitivities.
"""

import numpy as np


def summation(path: list,
              weight: np.ndarray) -> np.ndarray:
    """Summation of weights.

    Args:
        path (list): List of indices.
        weight (np.ndarray): List of weights.

    Returns:
        np.ndarray: Sum of weights.

    """
    if path:
        return np.sum(weight[path])
    else:
        return 0


def stouffers_method(path: list,
                     weight: np.ndarray) -> np.ndarray:
    """Stouffer's method for combination of p-values.

    Args:
        path (list): List of indices.
        weight (np.ndarray): List of weights.

    Returns:
        np.ndarray: Combined significances.

    """
    if path:
        return 1/np.sqrt(len(path)) * np.sum(weight[path])
    else:
        return 0
