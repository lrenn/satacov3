"""
Utils for plotting.
"""

from typing import Any, Dict, List, Union

import numpy as np
import pandas as pd


def __extract_likelihood_scan_indices(column_names: List[str]) -> Dict[str, Dict[str, List[Any]]]:
    """
    Extract the indices of the likelihood scans from the column names. Assuming that the column names are in the format
    'analysis__likelihood_scan__region' and that the column names are in the correct order.

    Args:
        column_names (List[str]): List of column names.

    Returns:
        Dict[str, Dict[str, List[Any]]]: Dictionary with likelihood scan indices.
    """
    likelihood_scans: Dict[str, Dict[str, List[Any]]] = {}
    for i, column_name in enumerate(column_names):
        # split column name at '__'
        split_column_name = column_name.split('__')

        # check if the likelihood scan is already in the dictionary
        if split_column_name[0] + '__' + split_column_name[1] not in likelihood_scans:
            likelihood_scans[split_column_name[0] + '__' +
                             split_column_name[1]] = {"region": [], "index": []}
        likelihood_scans[split_column_name[0] + '__' +
                         split_column_name[1]]["region"].append(split_column_name[2])
        likelihood_scans[split_column_name[0] +
                         '__' + split_column_name[1]]["index"].append(i)

    return likelihood_scans


def __extract_analysis_indices(column_names: List[str]) -> Dict[str, List[Any]]:
    """
    Extract the indices of the analyses from the column names. Assuming that the column names are in the format
    'analysis__likelihood_scan__region' and that the column names are in the correct order.

    Args:
        column_names (List[str]): List of column names.

    Returns:
        Dict[str, List[Any]]: Dictionary with analysis indices.
    """
    analyses: Dict[str, List[Any]] = {}
    for i, column_name in enumerate(column_names):
        # split column name at '__'
        split_column_name = column_name.split('__')

        # check if the analysis is already in the dictionary
        if split_column_name[0] not in analyses:
            analyses[split_column_name[0]] = []
        analyses[split_column_name[0]].append(i)

    return analyses


def __pretty_event_number(event_number: str) -> str:
    """
    Transform event number to pretty string.

    Args:
        event_number (str): Event number.

    Returns:
        str: Pretty event number.
    """
    # convert to int
    event_number: int = int(event_number)

    # event number should be written in scientific notation
    if event_number >= 1e6:
        return "{:.2e}".format(event_number)
    else:
        return str(event_number)


def __check_parts_validity(parts: Union[List[float], int, np.ndarray]) -> np.ndarray:
    """
    Check if parts are valid.

    Args:
        parts (Union[List[float], int, np.ndarray]): Parts of the data to plot.

    Returns:
        np.ndarray: Parts of the data to plot.
    """
    intervals: np.ndarray = np.array([])
    if isinstance(parts, int):
        # create intervals
        intervals = np.linspace(0, 1, parts + 1)

    elif isinstance(parts, list):
        # check if parts are in the range [0, 1]
        if not all([0 <= part <= 1 for part in parts]):
            raise ValueError(
                f"Parts {parts} are not in the range [0, 1].")

        # check if parts are sorted
        if not all([parts[i] <= parts[i + 1] for i in range(len(parts) - 1)]):
            raise ValueError(
                f"Parts {parts} are not sorted.")

        # set intervals
        intervals = np.array(parts)

    elif isinstance(parts, np.ndarray):
        # check if parts are in the range [0, 1]
        if not all([0 <= part <= 1 for part in parts]):
            raise ValueError(
                f"Parts {parts} are not in the range [0, 1].")

        # check if parts are sorted
        if not all([parts[i] <= parts[i + 1] for i in range(len(parts) - 1)]):
            raise ValueError(
                f"Parts {parts} are not sorted.")

        # set intervals
        intervals = parts

    else:
        raise ValueError(
            f"Type of parts {type(parts)} is not supported.")

    return intervals


def __check_variables_validity(df: pd.DataFrame,
                               xvar: str,
                               yvar: str) -> None:
    """
    Check if xvar and yvar are in the dataframe.

    Args:
        df (pd.DataFrame): Dataframe containing the data to plot.
        xvar (str): Name of the x-axis variable.
        yvar (str): Name of the y-axis variable.

    Returns:
        None
    """
    # check if xvar and yvar are in the dataframe
    if xvar not in df.columns:
        raise ValueError(
            f"Variable {xvar} not in dataframe columns {df.columns}.")
    if yvar not in df.columns:
        raise ValueError(
            f"Variable {yvar} not in dataframe columns {df.columns}.")
    return
