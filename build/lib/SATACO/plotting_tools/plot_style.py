"""
Plotting style for SATACO.
"""


import matplotlib.pyplot as plt
import mplhep as hep
from matplotlib.colors import LinearSegmentedColormap

COLORS = ["#F1BB7B", "#FD6467", "#5B1A18", "#D67236"]
CMAP = LinearSegmentedColormap.from_list("custom_cmap", COLORS[:-1], N=256)
ATLAS_TEXT = r"$\sqrt{s} = 13$ TeV, $139$ fb$^{-1}$"


def set_plot_style(matrix: bool = False) -> None:
    """
    Set the plotting style to ATLAS.

    Args:
        matrix: If True, set special matrix style.

    Returns:
        None
    """
    plt.style.use([hep.style.ROOT, hep.style.firamath])
    hep.atlas.label(label="Private Work", lumi=139.0,
                    year=2018, com=13, pad=3)
    plt.rcParams["font.family"] = "sans-serif"
    plt.rcParams["axes.labelweight"] = "bold"
    plt.rcParams["axes.labelsize"] = 18
    plt.rcParams["xtick.labelsize"] = 14
    plt.rcParams["ytick.labelsize"] = 14
    plt.rcParams["legend.fontsize"] = 14
    plt.rcParams["legend.handlelength"] = 1.5
    plt.rcParams["legend.borderaxespad"] = 0.5
    plt.rcParams["legend.borderpad"] = 0.5
    plt.rcParams["legend.frameon"] = True
    plt.rcParams["text.usetex"] = True
    plt.rcParams["ytick.major.size"] = 6
    plt.rcParams["xtick.major.size"] = 6
    plt.rcParams["ytick.minor.size"] = 3
    plt.rcParams["xtick.minor.size"] = 3
    plt.rcParams["xaxis.labellocation"] = "center"
    plt.rcParams["yaxis.labellocation"] = "center"
    plt.rcParams["xtick.direction"] = "out"
    plt.rcParams["ytick.direction"] = "out"

    if matrix:
        plt.rcParams["xtick.major.size"] = 7
        plt.rcParams["xtick.major.width"] = 1
        plt.rcParams["ytick.major.size"] = 7
        plt.rcParams["ytick.major.width"] = 1
        plt.rcParams["ytick.minor.size"] = 0
        plt.rcParams["xtick.minor.size"] = 0
    print("Plotting style set to ATLAS")
    return
