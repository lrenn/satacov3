''' Data thats useful for plotting '''

# ====================  Default ranges for different parameters ==================== #
range_dict = {
	'MO_Omega':(1e-4,1e0),
	'm_chi_10':(1e1,3e2),
	'm_chi_20':(1e2,3e2),
	'm_chi_1p':(1e1,5e2),
	'm_slep':(1e1,1200),
	'm_chi_2p':(1e1,5e2),
	'delta_m_chi_20_m_chi_10':(1e0,1e2),
	'delta_m_chi_1p_m_chi_10':(1e0,1e2),
	'delta_m_chi_1p_m_chi_20':(1e0,1e2),
	'delta_m_slep_m_chi_10':(1e0,1e2),
	'mA'      :(1e1,1e3),
	'mu'      :(5e1,2e3),
	'xsec_1p_1m':(1e-7,.5),
	'xsec_20_1m':(1e-7,.5),
	}

# ==================== Map from dataframe column names to axis labels ==================== #
particle_names = {
	'LSP' : 'LSP',
	'chi_10':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{0}}$',
	'chi_20':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{2}}^{\\mathdefault{0}}$',
	'chi_30':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{3}}^{\\mathdefault{0}}$',
	'chi_40':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{4}}^{\\mathdefault{0}}$',
	'chi_1p':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{+}}$',
	'chi_2p':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{2}}^{\\mathdefault{+}}$',
	'chi_1m':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{-}}$',
	'chi_2m':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{2}}^{\\mathdefault{-}}$',
	'chi_1pm':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{\pm}}$',
	'gam':'$\\gamma$',

	'e_L'  : '$\\tilde{e}_{L}$',
	'e_R'  : '$\\tilde{e}_{R}$',
	'sele'  : '$\\tilde{e}_{L,R}$',
	'nu_eL' : '$\\tilde{\\nu}_{e_{L}}$',

	'mu_L': '$\\tilde{\\mu}_{L}$',
	'mu_R': '$\\tilde{\\mu}_{R}$',
	'smu'   : '$\\tilde{\\mu}_{L,R}$',
	'nu_muL'  : '$\\tilde{\\nu}_{\\mu_{L}}$',

	'slep_L': '$\\tilde{\\ell}_{L}$',
	'slep_R': '$\\tilde{\\ell}_{R}$',
	'slep' : '$\\tilde{\\ell}_{L,R}$',

	'tau_1' : '$\\tilde{\\tau}_{1}$',
	'tau_2' : '$\\tilde{\\tau}_{2}$',
	'stau'  : '$\\tilde{\\tau}_{1,2}$',
	'nu_tauL': '$\\tilde{\\nu}_{\\tau_{L}}$',
	'Z' : 'Z',
	'W' : 'W',
	'h' : 'h',
	}
masses = {'m_'+p:'$\\mathrm{m}('+particle_names[p][1:-1]+')$ [GeV]' for p in particle_names}
masses['m_smu'] = '$\\min(\\mathrm{m}(\\tilde{\\mu}_{L}),\\mathrm{m}(\\tilde{\\mu}_{R}))$'
masses['m_sele'] = '$\\min(\\mathrm{m}(\\tilde{e}_{L}),\\mathrm{m}(\\tilde{e}_{R}))$'
masses['m_slep'] = '$\\min(\\mathrm{m}(\\tilde{\\ell}_{L}),\\mathrm{m}(\\tilde{\\ell}_{R}))$'
masses['m_stau'] = '$\\min(\\mathrm{m}(\\tilde{\\tau}_{1}),\\mathrm{m}(\\tilde{\\tau}_{2}))$'
masses['m_h'] = '$\\mathrm{m}(\\mathrm{h})$ [GeV]'
masses['value_m_h'] = '$\\mathrm{m}(\\mathrm{h})$ [GeV]'
masses['m_H'] = '$\\mathrm{m}\\mathrm{(H})$ [GeV]'
masses['W_mass'] = '$\\mathrm{m}\\mathrm{(W})$ [GeV]'
masses['W_mass'] = '$\\mathrm{m}\\mathrm{(W})$ [GeV]'
masses['value_m_w_mssm'] = '$\\mathrm{m}\\mathrm{(W})$ [GeV]'
masses['m_A'] = '$\\mathrm{m}(\\mathrm{A})$ [GeV]'
masses['min_slep_C1'] = '$\\min(\\mathrm{m}(\\ell_{L}),\\mathrm{m}(\\ell_{R}), \\mathrm{m}(\\mathrm{\\chi}_1^+))$'
masses['m_wino'] = '$\\mathrm{m}(W)$'
other_params = {
	'MO_Omega':'$\\Omega h^2$',
	'mA':'$\\mathrm{m}_{\\mathrm{A}}$ [GeV]',
	'value_delta_rho':'$\\Delta\\rho$',
	'Z_mssm_width':'$\\Gamma_{\\mathrm{inv.}}(Z)$ [MeV]',
	}
delta_masses = {}
xsecs = {}
for p in particle_names:
	for q in particle_names:
		delta_masses['delta_m_'+p+'_m_'+q] = '$\\Delta \\mathrm{m}('+particle_names[p][1:-1]+', '+particle_names[q][1:-1]+')$ [GeV]'
		xsecs['xsec_'+p[4:]+'_'+q[4:]] = '$\\sigma('+particle_names[p][1:-1]+particle_names[q][1:-1]+')$ pb'
xsecs['xsec_20_1pm'] = '$\\sigma(\\tilde{\\mathrm{\\chi}}_{\\mathdefault{2}^0\\tilde{\\mathrm{\\chi}}_1^{\\pm})$ pb'
xsecs['xsec_10_1pm'] = '$\\sigma(\\tilde{\\mathrm{\\chi}}_1^0\\tilde{\\mathrm{\\chi}}_1^{\\pm})$ pb'
fractions = {}
for p in ['LSP','chi_20','chi_30','chi_40','chi_1p','chi_1m','chi_2p','chi_2m']:
	for q in ['Wino','Bino','Higgsino']:
		fractions[p+'_'+q+'_frac'] = particle_names[p]+' '+q+' fraction'
xsec_ratios = {}
for x in xsecs:
	for L in ['wino','bino','higgsino']:
		#xsec_ratios[x+'_'+L+'_ratio'] = '$'+xsecs[x][1:-4]+' / \\sigma($Simplified '+L+' model$)$'
		xsec_ratios[x+'_'+L+'_ratio'] = '$\\sigma_{\\mathrm{pMSSM}} / \\sigma_{\\mathrm{Simpf.\ '+L+'\ model}}$ ($'+xsecs[x][8:-5]+'$ prod.)'
BF_columns = [
'BF_chi_1p_to_chi_10', 'BF_chi_1p_to_chi_20',
'BF_chi_20_to_chi_10', 'BF_chi_20_to_chi_10_Z',
'BF_chi_20_to_chi_10_h', 'BF_chi_20_to_chi_1p',
'BF_chi_20_to_chi_1p_W', 'BF_chi_20_to_chi_2p',
'BF_chi_20_to_chi_2p_W', 'BF_chi_2p_to_chi_10',
'BF_chi_2p_to_chi_1p', 'BF_chi_2p_to_chi_1p_Z',
'BF_chi_2p_to_chi_1p_h', 'BF_chi_2p_to_chi_20',
'BF_h_to_b_b', 'BF_H_to_b_b',
'BF_chi_20_to_chi_10_ele_ele','BF_chi_20_to_chi_10_mu_mu','BF_chi_20_to_chi_10_tau_tau',
'BF_chi_20_to_chi_1p_ele_nu_ele','BF_chi_20_to_chi_1p_mu_nu_mu','BF_chi_20_to_chi_1p_tau_nu_tau',
'BF_chi_20_to_chi_10_gam',
]
BFs = {}
for BF in BF_columns:
	splitBF = BF[3:].split('_to_')
	if splitBF[0] in particle_names:
		particle1 = particle_names[splitBF[0]].replace('$','')
	else:
		particle1 = splitBF[0]
	if splitBF[1] in particle_names:
		particle2 = splitBF[1]
		out = particle_names[particle2].replace('$','')
	elif splitBF[1][:-2] in particle_names:
		particle2 = splitBF[1][:-2]
		out = particle_names[particle2].replace('$','')+' + '+particle_names[splitBF[1].split('_')[-1]].replace('$','')
	elif splitBF[1][:-4] in particle_names:
		particle2 = splitBF[1][:-4]
		out = particle_names[particle2].replace('$','')+' + '+particle_names[splitBF[1].split('_')[-1]].replace('$','')
	else:
		particle2=splitBF[1].split('_')
		out = ''
		for p in particle2:
			out+=p

	BFs[BF] = 'BR($'+particle1+'\\rightarrow '+out+'$)'
# manually put in some extra BFs
BFs['BF_chi_20_to_chi_10_ele_ele'] = 'BR('+particle_names['chi_20']+'$\\rightarrow$'+particle_names['chi_10']+'$e^+e^-$)'
BFs['BF_chi_20_to_chi_10_mu_mu'] = 'BR('+particle_names['chi_20']+'$\\rightarrow$'+particle_names['chi_10']+'$\\mu^+\\mu^-$)'
BFs['BF_chi_20_to_chi_10_tau_tau'] = 'BR('+particle_names['chi_20']+'$\\rightarrow$'+particle_names['chi_10']+'$\\tau^+\\tau^-$)'
BFs['BF_chi_20_to_chi_10_ele_nu_ele'] = 'BR('+particle_names['chi_20']+'$\\rightarrow$'+particle_names['chi_1p']+'$e^+\\nu_e$)'
BFs['BF_chi_20_to_chi_10_mu_nu_mu'] = 'BR('+particle_names['chi_20']+'$\\rightarrow$'+particle_names['chi_1p']+'$\\mu^+\\nu_{\\mu}$)'
BFs['BF_chi_20_to_chi_10_tau_nu_tau'] = 'BR('+particle_names['chi_20']+'$\\rightarrow$'+particle_names['chi_1p']+'$\\tau^+\\nu_{\\tau}$)'
BFs['BF_h_to_chi_10_chi_10'] = 'BR$(h\\rightarrow$'+particle_names['chi_10']+particle_names['chi_10']+'$)$'
BFs['BF_h_to_inv'] = 'BR$(h\\rightarrow \\mathrm{inv.})$'
BFs['BR_b_to_sgamma'] = 'BR$(b\\rightarrow s\\mathrm{\\gamma})$'
BFs['BR_Bs_to_mumu'] = 'BR$(B_s\\rightarrow \\mathrm{\\mu}\\mathrm{\\mu})$'
BFs['BR_Bu_to_taunu'] = 'BR$(B_u\\rightarrow \\mathrm{\\tau}\\mathrm{\\nu}_{\\mathrm{\\tau}})$'
BFs['SI_BR_b_to_sgamma'] = 'BR$(b\\rightarrow s\\mathrm{\\gamma})$'
BFs['SI_BR_Bs_to_mumu'] = 'BR$(B_s\\rightarrow \\mathrm{\\mu}\\mathrm{\\mu})$'
BFs['SI_BR_Bu_to_taunu'] = 'BR$(B_u\\rightarrow \\mathrm{\\tau}\\mathrm{\\nu}_{\\mathrm{\\tau}})$'

label_map = {**particle_names, **masses,**other_params,**delta_masses,**xsecs,**fractions,**xsec_ratios,**BFs}
for F in ['DF','SF']:
	for J in ['0J','1J']:
		for L in ['a','b','c','d']:
			label_map['EWSmear_acceptance_EwkTwoLeptonZeroJet2018__'+F+'_'+J+'_Incl_'+L] = F+J+'_'+L+' acceptance'
			label_map['EWSmear_events_EwkTwoLeptonZeroJet2018__'+F+'_'+J+'_Incl_'+L] = F+J+'_'+L+' yield'
			label_map['EWSmear_ObsCLs_EwkTwoLeptonZeroJet2018__'+F+'_'+J+'_Incl_'+L] = F+J+'_'+L+' ObsCLs'
			label_map['EWSmear_ExpCLs_EwkTwoLeptonZeroJet2018__'+F+'_'+J+'_Incl_'+L] = F+J+'_'+L+' ExpCLs'

for S in ['High','Med','Low']:
	label_map['EWSmear_events_EwkOneLeptonTwoBjets2018__SR_h_'+S+'_Incl'] = '1Lbb '+S+' incl. SR yield'
	label_map['EWSmear_acceptance_EwkOneLeptonTwoBjets2018__SR_h_'+S+'_Incl']= '1Lbb '+S+' incl. SR acceptance'
	label_map['EWSmear_ObsCLs_EwkOneLeptonTwoBjets2018__SR_h_'+S+'_Incl'] = '1Lbb '+S+' incl. SR ObsCLs'
	label_map['EWSmear_ExpCLs_EwkOneLeptonTwoBjets2018__SR_h_'+S+'_Incl'] = '1Lbb '+S+' incl. SR ExpCLs'
label_map['EWSmear_events_EwkCompressed2018_withBins__SR_E_iMLLi'] = 'Compressed SR_E_iMLLi yield'
label_map['EWSmear_acceptance_EwkCompressed2018_withBins__SR_E_iMLLi'] = 'Compressed SR_E_iMLLi acceptance'

label_map['GM2_gmuon'] = '$\\Delta a_{\\mu}$ (GM2Calc)'
label_map['gmuon'] = '$\\Delta \\mathrm{a}_{\\mu}$ (SPfh)'
label_map['MO_proton_SI'] = particle_names['chi_10']+'-proton $\\sigma_{\\mathrm{SI}}$ [cm$^2$]'
label_map['MO_neutron_SI'] = particle_names['chi_10']+'-neutron $\\sigma_{\\mathrm{SI}}$ [cm$^2$]'
label_map['MO_nucleon_SI'] = particle_names['chi_10']+'-nucleon $\\sigma_{\\mathrm{SI}}$ [cm$^2$]'
label_map['MO_proton_SD'] = particle_names['chi_10']+'-proton $\\sigma_{\\mathrm{SD}}$ [cm$^2$]'
label_map['MO_neutron_SD'] = particle_names['chi_10']+'-neutron $\\sigma_{\\mathrm{SD}}$ [cm$^2$]'
label_map['WIMP_proton_SI'] = 'WIMP-proton $\\sigma_{\\mathrm{SI}}$ [cm$^2$]'
label_map['WIMP_neutron_SI'] = 'WIMP-neutron $\\sigma_{\\mathrm{SI}}$ [cm$^2$]'
label_map['WIMP_nucleon_SI'] = 'WIMP-nucleon $\\sigma_{\\mathrm{SI}}$ [cm$^2$]'
label_map['WIMP_proton_SD'] = 'WIMP-proton $\\sigma_{\\mathrm{SD}}$ [cm$^2$]'
label_map['WIMP_neutron_SD'] = 'WIMP-neutron $\\sigma_{\\mathrm{SD}}$ [cm$^2$]'

label_map['mu'] = '$\\mathrm{\\mu}$'
label_map['M_1'] = '$M_1$'
label_map['M_2'] = '$M_2$'
label_map['M_3'] = '$M_3$'
label_map['tanb_ext'] = '$\\tan\\mathrm{\\beta}$'
label_map['Filter_eff'] = 'Filter efficiency'
label_map['N_filt_need'] = 'Ideal number of filtered events'
label_map['N_need'] = 'Ideal number of events'
label_map['N_ratio'] = '$N_{\\mathrm{events}}$ / $N_{\\mathrm{events}}^{\\mathrm{Ideal}}$'
label_map['OverallExpCLs'] = 'Overall CL$_s^{\\mathrm{Exp.}}$'
label_map['OverallObsCLs'] = 'Overall CL$_s^{\\mathrm{Obs.}}$'
label_map['tau_chi_1p'] = '$\\tau(\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\pm})$ [ns]'
#label_map['DT_xsec_limit'] = 'Disappearing track 95% CL$_s$ \nupper limits on cross-section [fb]'
label_map['DT_xsec_limit'] = 'Disappearing track $\\sigma_{\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\pm}\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{0}}$ limit [fb]'

# ==================== Map from DM category to a color for 2D plotting ==================== #
DM_color_map = {
	label_map['chi_1p']+' coann.'  : 'tab:orange',
	label_map['chi_20']+' coann.'  : 'tab:blue',
	label_map['chi_1p']+'/'+label_map['chi_20']+' self/co. ann.' : 'tab:orange',
	label_map['chi_1p']+' / '+label_map['chi_20']+' co. ann.' : 'tab:orange',
	label_map['stau']+' coann.'    : 'tab:cyan',
	label_map['stau']+' self/co. ann.'    : 'tab:cyan',
	label_map['stau']+' co. ann.'    : 'tab:cyan',

	'$\\tilde{\\ell}$ self/co. ann.' : 'tab:blue',
	label_map['mu_R']+' coann.'    : 'tab:red',
	label_map['smu']+' coann.'     : 'tab:red',
	label_map['e_R']+' coann.'     : 'tab:pink',
	label_map['sele']+' coann.'    : 'tab:pink',
	'$\\tilde{\\nu}$ coann.'       : 'green',
	'$\\tilde{\\nu}$ self/co. ann.': 'khaki',
	'$\\tilde{\\nu}$ co. ann.': 'khaki',
	label_map['nu_tauL']+' coann.' : 'chartreuse',
	label_map['nu_muL']+' coann.'  : 'green',
	label_map['nu_eL']+' coann.'   : 'lightgreen',

	'$\\mathrm{\\chi}_{\\mathdefault{1}}^{\\mathdefault{0}} \\mathrm{\\chi}_{\\mathdefault{1}}^{\\mathdefault{0}} \\rightarrow$ Z h' : 'tab:pink',
	'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{0}} \\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{0}} \\rightarrow$ Z h' : 'tab:pink',
	'$\\mathrm{\\chi}_{\\mathdefault{1}}^{\\mathdefault{0}} \\mathrm{\\chi}_{\\mathdefault{1}}^{\\mathdefault{0}} \\rightarrow W^+ W^-$' : 'tab:red',
	'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{0}} \\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{0}} \\rightarrow W^+ W^-$' : 'tab:red',
	'$\\mathrm{\\chi}_{\\mathdefault{1}}^+$/$\\mathrm{\\chi}_{\\mathdefault{2}^{\\mathdefault{0}}$ self/co. ann.':'khaki',
	'$\\mathrm{\\chi}_{\\mathdefault{1}}^+$ / $\\mathrm{\\chi}_{\\mathdefault{2}^{\\mathdefault{0}}$ co. ann.':'khaki',
	'$\\mathrm{\\chi}_{\\mathdefault{1}}^{\\mathdefault{0}} \\mathrm{\\chi}_{\\mathdefault{1}}^{\\mathdefault{0}} \\rightarrow t \\bar{t}$' : 'deeppink',
	'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{0}} \\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{0}} \\rightarrow t \\bar{t}$' : 'deeppink',
	'$\\mathrm{\\chi}_{\\mathdefault{1}}^{\\mathdefault{0}} \\mathrm{\\chi}_{\\mathdefault{1}}^{\\mathdefault{0}} \\rightarrow b \\bar{b}$' : 'tab:red',
	'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{0}} \\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{0}} \\rightarrow b \\bar{b}$' : 'tab:red',
	'$\\mathrm{\\chi}_{\\mathdefault{1}}^{\\mathdefault{0}} \\mathrm{\\chi}_{\\mathdefault{1}}^{\\mathdefault{0}} \\rightarrow ZZ$':'tab:grey',
	'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{0}} \\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{0}} \\rightarrow ZZ$':'tab:grey',
	'$\\mathrm{\\chi}_{\\mathdefault{1}}^{\\mathdefault{0}} \\mathrm{\\chi}_{\\mathdefault{1}}^{\\mathdefault{0}} \\rightarrow$ VV':'tab:grey',
	'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{0}} \\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{0}} \\rightarrow$ VV':'tab:grey',
	'$\\tilde{\\ell}$/$\\tilde{\\nu}$ self/co. ann.' : 'forestgreen',
	'$\\tilde{\\ell}$/$\\tilde{\\nu}$ co. ann.' : 'forestgreen',
        'Z/h funnel'                    : 'purple',
        'Z funnel'                      : 'purple',
        'h funnel'                      : 'purple',
        'H funnel'                      : 'tab:green',
	'A funnel'                      : 'lightgreen',#tab:olive',
	'A/H funnel'                    : 'tab:green',#tab:olive',
	'Other $\\mathrm{\\chi}_1^0 \\mathrm{\\chi}_1^0 \\rightarrow$ X X' : 'black',
	'Other $\\tilde{\\mathrm{\\chi}}_1^0 \\tilde{\\mathrm{\\chi}}_1^0 \\rightarrow$ X X' : 'black',
	'Other funnel'                  : 'khaki',
	'Ann. to tau'                   : 'indianred',
	't-chan sparticle exch.'        : 'tab:gray',
	'other'                         : 'black',
	'Other'                         : 'black',
	'Other N1N1'                    : 'deeppink',
	'Other N1N1*'                    : 'deeppink',

	# add nLSP as well
	'chi_1p' : 'tab:orange',
	'chi_20' : 'tab:blue',
	'stau'   : 'tab:cyan',
	'mu_R'   : 'tab:red',
	'smu'    : 'tab:red',
	'e_R'    : 'pink',
	'sele'   : 'pink',
	'nu_tauL': 'chartreuse',
	'nu_muL' : 'green',
	'nu_eL'  : 'lightgreen',
}
