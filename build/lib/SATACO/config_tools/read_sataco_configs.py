"""
Reading configuration file that is specified either by the user or by default
in the pMSSMFactory.
"""

import configparser
from typing import Any, Dict

from .check_sataco_configs import check_sataco_configs


def read_sataco_configs(path_config_file: str,
                        section: str = 'DEFAULT') -> Dict[str, Any]:
    """
    Read the configuration file that is specified either by the user or by
    default in the pMSSMFactory.

    Args:
        config_file (str): The path to the configuration file.
        section (str): Section of the config file. Defaults to 'DEFAULT'.

    Returns:
        (Dict[str, Any]): A dictionary containing the values from the
            configuration file.
    """
    # create an instance of the configparser object
    config: configparser.ConfigParser = configparser.ConfigParser()

    try:
        # read the config file
        config.read(path_config_file)
    except configparser.MissingSectionHeaderError:
        print('The config file is empty. Please provide a config file.')
        raise SystemExit(0)
    except FileNotFoundError:
        print('The config file does not exist. Please provide a config file.')
        raise SystemExit(0)

    # access the values from the config file
    try:
        config_values: Dict = {
            'weight_method': config.get(
                section=section,
                option='weight_method'),
            'correlation_method': config.get(
                section=section,
                option='correlation_method'),
            'confidence_level': config.getfloat(
                section=section,
                option='confidence_level'),
            'correlation_threshold': config.getfloat(
                section=section,
                option='correlation_threshold'),
            'combination_method': config.get(
                section=section,
                option='combination_method'),
        }
    except configparser.NoOptionError:
        print('The config file is missing an option. '
              'Please provide a valid config file.')
        raise SystemExit(0)
    except configparser.NoSectionError:
        print('The config file is this section. '
              'Please provide a valid config file.')
        raise SystemExit(0)
    except ValueError:
        print(
            'The config file contains an invalid value. '
            'Please provide a valid config file.')
        raise SystemExit(0)

    # further checks on the types and values of the config file arguments
    if not check_sataco_configs(config_values):
        print('The config file contains invalid arguments. '
              'Please provide a valid config file.')
        raise SystemExit(0)

    return config_values
