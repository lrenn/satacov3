"""
The functions here will be used to infer the 19 pMSSM parameters.
"""

from typing import Callable, Dict, List

import numpy as np
import pandas as pd
from skopt.acquisition import gaussian_ei, gaussian_lcb, gaussian_pi
from skopt.optimizer import (Optimizer, dummy_minimize, forest_minimize,
                             gbrt_minimize, gp_minimize)
from skopt.space import Real


def learn_pMSSM_points(df_pmssm_eff: pd.DataFrame,
                       dimension_borders: Dict[str, Real],
                       acquisition_function: str = "EI",
                       minimization_function: str = "GP",
                       acquisition_optimizer: str = "LBFGS",
                       num_next_points: int = 5) -> pd.DataFrame:
    """
    Learn with gradient free optimization techniques the next best 
    points to get high signal efficiencies.
    """
    # Inferred parameter dimensions into list
    dimensions: List[Real] = [value for _,
                              value in sorted(dimension_borders.items())]

    # Parameters in sorted list
    parameter_names: List[str] = [key for key,
                                  _ in sorted(dimension_borders.items())]

    # Initialize optimizer without any functions and values
    optimizer: Optimizer = Optimizer(
        dimensions=dimensions,
        base_estimator=minimization_function.lower(),
        acq_func=acquisition_function,
        acq_optimizer=acquisition_optimizer.lower(),
        n_initial_points=0
    )

    # Tell the optimizer all the evaluated points
    x: List
    y: List

    try:
        # put the parameters in an array
        x = np.array(df_pmssm_eff[parameter_names]).tolist()

        # put the negative signal efficiencies
        y = np.array(df_pmssm_eff['neg_eff']).tolist()

    except KeyError:
        raise KeyError("The parameter is not in the dataframe.")

    # Chunk down the x and y arrays into smaller arrays and feed the optimizer
    # in a loop
    chunk_size: int = 50
    chunked_x: List = [x[i:i + chunk_size]
                       for i in range(0, len(x), chunk_size)]
    chunked_y: List = [y[i:i + chunk_size]
                       for i in range(0, len(y), chunk_size)]

    # Loop over the chunks
    for idx, (x_chunk, y_chunk) in enumerate(zip(chunked_x, chunked_y)):
        print(f"Chunk {idx+1}/{len(chunked_x)}", end="\r")

        # Tell the optimizer the evaluated points
        optimizer.tell(x_chunk, y_chunk)

    # Ask for the next points
    num_exploration_points: int = num_next_points - 1
    exploration_points: List = []
    if num_exploration_points > 0:
        exploration_points = optimizer.ask(n_points=num_exploration_points)
    eploitation_points: List = [optimizer.get_result()["x"]]
    next_pmssm_points: List = exploration_points + eploitation_points

    # Put the next points in a dataframe
    df_next_pmssm_points: pd.DataFrame = pd.DataFrame(
        next_pmssm_points, columns=parameter_names)

    return df_next_pmssm_points


def merge_efficiencies_2_parameters(efficiencies_df: pd.DataFrame,
                                    parameters_df: pd.DataFrame,
                                    combination: str,
                                    signal_eff_alpha: float = 0.9) -> pd.DataFrame:
    """
    Function merges the dataframe where the accepted events are stored per signal region
    and the dataframe that contains the 19 pMSSM parameters for each model point.

    efficiencies_df:
    ----------------------------------------------------------------
    | Model | <Analysis>__<SRi> | ... | <Analysis>__<SRk> |    n   |
    ----------------------------------------------------------------
    |  1    | 0                 | ... | 20                | 100000 |
    ...
    ----------------------------------------------------------------

    parameters_df:
    -----------------------------
    | Model | M_1  | ... | tanb |
    -----------------------------
    |  1    | 20.0 | ... | -1    |
    ...
    -----------------------------

    Args:
        efficiencies_df (pd.DataFrame): Dataframe containing analyses
            or signal regions with the accepted events after
            the TruthAcceptance.
        parameters_df (pd.DataFrame): Dataframe containing parameters
            for each model point.
        combination (str): A valid combination of two SRs or Analyses in
            the form of <SR_x>&<SR_y>.
        signal_eff_alpha (float): Alpha parameter for the signal
            efficiency function. Defaults to 0.9.

    Return:
        (pd.DataFrame): Merge the dataframes into one with the
            selected SRs or Analyses and appended efficiency.

    """
    # Check if the combination is valid
    if len(combination.split("&")) != 2:
        raise ValueError(
            "Combination argument is invalid. Format may be incorrect.")

    # Check if these keys are in the dataframe
    if (combination.split("&")[0] not in efficiencies_df.columns
            or combination.split("&")[1] not in efficiencies_df.columns):
        raise KeyError(
            f"Combination contains keys {combination.split('&')[0]} and "
            f"{combination.split('&')[1]} "
            "that are not in the dataframe.")

    # Set the correct function
    neg_eff_func: Callable = __negative_signal_efficiency_function

    # Merge the dataframes on the model number and from the
    # efficiencies_df only the combination and the total number of events
    merged_df: pd.DataFrame = pd.merge(
        efficiencies_df[['Model', combination.split("&")[0],
                         combination.split("&")[1], 'n']],
        parameters_df,
        on='Model')

    # Calculate the negative signal efficiency for
    # every model point and append it to the dataframe
    # Arguments are the yields for the respective signal region/analysis,
    # the total number of the model point
    merged_df['neg_eff'] = neg_eff_func(
        yields_i=merged_df[combination.split("&")[0]],
        yields_j=merged_df[combination.split("&")[
            1]],
        n_total=merged_df["n"],
        signal_eff_alpha=signal_eff_alpha)

    return merged_df


def retrieve_dimension_borders(
        parameter_dataframe: pd.DataFrame,
        pMSSM_parameters: List[str],
        smear_borders: float = 0.01) -> Dict[str, Real]:
    """
    Function will retrieve the dimension borders for the active learning process.

    The structure of the parameter_dataframe should be as follows:
    Model,tanb,M_1,M_2,M_3,At,Ab,Atau,mu,mA,meL,mmuL,mtauL,meR,mmuR,mtauR,mqL1,mqL2,mqL3,muR,mcR,mtR,mdR,msR,mbR
    ...

    Args:
        parameter_dataframe (pd.DataFrame): Dataframe with the parameters for each model point.
        pMSSM_parameters (List[str]): List of pMSSM parameters that should be infered.
        smear_borders (float): Smear borders of the parameters such that solutions can also be found
            outside the parameters space that have been used before. Defaults to 0.01.

    """
    # Get it a minimum and maximum value for each pMSSM parameter that should be infered and
    # store them with the parameter name in a dictionary Real(min_value - min_value*smear_borders,
    # max_value + max_value*smear_borders)

    # Initialize the dictionary
    dimemsion_borders: Dict[str, Real] = {}

    # min max value
    min_value: float = None
    max_value: float = None

    # Loop over the pMSSM parameters
    try:
        for parameter in pMSSM_parameters:
            # Get the minimum and maximum value for each parameter
            min_value = float(parameter_dataframe[parameter].min())
            max_value = float(parameter_dataframe[parameter].max())

            # Store the values in the dictionary
            dimemsion_borders[parameter] = Real(min_value - abs(min_value * smear_borders),
                                                max_value +
                                                abs(max_value * smear_borders),
                                                prior="uniform",
                                                transform="identity")
    except KeyError:
        print("The parameter is not in the dataframe.")
        raise KeyError
    except TypeError:
        print("The parameter is not a float.")
        raise TypeError

    return dimemsion_borders


def all_pMSSM_parameters() -> List[str]:
    """
    Return all pMSSM parameters in a list.

    Returns:
        List[str]: List with all pMSSM parameters.
    """
    pMSSM_parameters: List[str] = [
        'tanb',
        'M_1',
        'M_2',
        'M_3',
        'At',
        'Ab',
        'Atau',
        'mu',
        'mA',
        'meL',
        'mmuL',
        'mtauL',
        'meR',
        'mmuR',
        'mtauR',
        'mqL1',
        'mqL2',
        'mqL3',
        'muR',
        'mcR',
        'mtR',
        'mdR',
        'msR',
        'mbR'
    ]

    return pMSSM_parameters


def restrict_parameter_length(inferrence_parameters: List[str]) -> List[str]:
    """
    Function will restrict the number of parameters that should be infered.

    mmuL = meL
    mmuR = meR
    mqL2 = mqL1
    mcR  = muR
    msR  = mdR

    Args:
        inferrence_parameters (List[str]): List of parameters that should be infered.

    Returns:
        List[str]: List of parameters that should be infered.
    """

    # Always infer the same parameters
    equivalent_parameters: Dict[str, str] = {
        'mmuL': 'meL',
        'mmuR': 'meR',
        'mqL2': 'mqL1',
        'mcR': 'muR',
        'msR': 'mdR'
    }

    # Exchange all equivalent parameters in the list
    for infer_param in inferrence_parameters:
        if infer_param in equivalent_parameters.keys():
            inferrence_parameters.remove(infer_param)
            inferrence_parameters.append(equivalent_parameters[infer_param])

    # Remove all duplicates from the list -> Reduced to 19 parameters
    inferrence_parameters = list(set(inferrence_parameters))

    # return the sorted list
    return sorted(inferrence_parameters)


def __negative_signal_efficiency_function(yields_i: np.ndarray,
                                          yields_j: np.ndarray,
                                          n_total: np.ndarray,
                                          signal_eff_alpha: float = 0.5) -> np.ndarray:
    """
    Function will calculate the negative signal efficiency for a given model point.
    Will be used on a dataframe with the yields for each signal region/analysis.

    Args:
        yields_i (np.ndarray): Yields for signal region/analysis i.
        yields_j (np.ndarray): Yields for signal region/analysis j.
        n_total (np.ndarray): Total number of events for a given model point.
        signal_eff_alpha (float): Alpha parameter for the signal efficiency function. Defaults to 0.9.

    Returns:
        np.ndarray: Array with the negative signal efficiency for each model point.
    """
    # Term for high signal efficiencies in both signal regions (quadradic)
    term_1: np.ndarray = signal_eff_alpha * \
        (yields_i / n_total)**2 + (yields_j / n_total)**2

    # Term for punishing very different signal efficiencies
    term_2: np.ndarray = (1 - signal_eff_alpha) * \
        (1 - (yields_i / n_total)**2 + (yields_j / n_total)**2)

    # NOTE: In general it is rather wanted to have a high alpha because if alpha is
    # too low the function can also find a minimum for low efficienciecies which is contradictory
    # to the first term.

    return - (term_1 + term_2)
