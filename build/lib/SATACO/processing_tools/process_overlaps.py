"""
All overlap methods are implemented here.
"""

import numpy as np


def pearson_correlation(**kwargs) -> np.ndarray:
    """
    Calculate the pearson correlation coefficient for the given
    signal regions/analyses.

    Args:
        **kwargs: Arbitrary keyword arguments.

    Returns:
        np.ndarray: The pearson correlation coefficient matrix.
    """

    # get the arguments
    n = kwargs['n']
    sum_x_y = kwargs['sum_x_y']
    sum_x = kwargs['sum_x']
    sum_x_2 = kwargs['sum_x_2']

    matrix = (n * sum_x_y - np.ones_like(sum_x_y) * sum_x *
              np.array(np.ones_like(sum_x_y) * sum_x).T) / (np.sqrt(n * np.ones_like(sum_x_y)
                                                                    * sum_x_2 - np.ones_like(sum_x_y) * sum_x**2) *
                                                            np.sqrt(n * np.array(np.ones_like(sum_x_y) * sum_x_2).T -
                                                                    np.array(np.ones_like(sum_x_y) * sum_x**2).T))

    return matrix


def geometrical_index(**kwargs) -> np.ndarray:
    """Calculate the geometrical correlation coefficient for the given
    signal regions/analyses. (The same as jaccard index)

    Args:
        **kwargs: Arbitrary keyword arguments.

    Returns:
        np.ndarray: The geometrical correlation coefficient matrix.
    """

    # get the arguments
    sum_x = kwargs['sum_x']
    sum_x_y = kwargs['sum_x_y']

    # calculate the geometrical index
    matrix = sum_x_y / (np.ones_like(sum_x_y) * sum_x +
                        np.array(np.ones_like(sum_x_y) * sum_x).T - sum_x_y)

    return matrix


def jaccard_index(**kwargs) -> np.ndarray:
    """
    Calculate the Jaccard similarity index for the given
    signal regions/analyses. (Points to geometrical correlation coefficient,
    because the Jaccard index is the same as the geometrical correlation).

    Args:
        **kwargs: Arbitrary keyword arguments.

    Returns:
        np.ndarray: The Jaccard similarity index matrix.
    """
    return geometrical_index(**kwargs)


def sorensen_dice_index(**kwargs) -> np.ndarray:
    """Calculate the Sorensen-Dice index for the given
    signal regions/analyses.

    Args:
        **kwargs: Arbitrary keyword arguments.

    Returns:
        np.ndarray: The Sorensen-Dice index coefficient matrix.
    """

    # get the arguments
    sum_x_2 = kwargs['sum_x_2']
    sum_x_y = kwargs['sum_x_y']

    matrix = 2 * sum_x_y / (np.ones_like(sum_x_y) * sum_x_2 +
                            np.array(np.ones_like(sum_x_y) * sum_x_2).T)

    return matrix


def cosine_index(**kwargs) -> np.ndarray:
    """
    Calculate the cosine similarity index for the given
    signal regions/analyses.

    Args:
        **kwargs: Arbitrary keyword arguments.

    Returns:
        np.ndarray: The cosine similarity index matrix.
    """

    # get the arguments
    sum_x = kwargs['sum_x']
    sum_x_y = kwargs['sum_x_y']

    matrix = sum_x_y / np.sqrt(np.ones_like(sum_x_y) * sum_x *
                               np.array(np.ones_like(sum_x_y) * sum_x).T)

    return matrix


def otsuka_ochiai_index(**kwargs) -> np.ndarray:
    """
    Calculate the Otsuka-Ochiai similarity index for the given
    signal regions/analyses.

    Args:
        **kwargs: Arbitrary keyword arguments.

    Returns:
        np.ndarray: The Otsuka-Ochiai similarity index matrix.
    """

    # same as cosine correlation
    return cosine_index(**kwargs)


def szymkiewicz_simpson_index(**kwargs) -> np.ndarray:
    """
    Calculate the Szymkiewicz-Simpson similarity index for the given
    signal regions/analyses.

    Args:
        **kwargs: Arbitrary keyword arguments.

    Returns:
        np.ndarray: The Szymkiewicz-Simpson similarity index matrix.
    """

    # get the arguments
    sum_x = kwargs['sum_x']
    sum_x_y = kwargs['sum_x_y']

    matrix = sum_x_y / np.minimum(np.ones_like(sum_x_y) * sum_x,
                                  np.array(np.ones_like(sum_x_y) * sum_x).T)

    return matrix
