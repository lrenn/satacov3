"""
Transform the results from the pMSSM Factory into pandas dataframes fromat
that will be used for plotting.
This is heavily inspired by Benjamin Hodkinson work for EWK pMSSM results.
Moreover, this is an extreme simplification, i.e. only serves the purpose of
plotting quickly the results in the pMSSM Factory.
"""

import os
from typing import Any, Dict, List, Literal, Tuple
from warnings import simplefilter

import pandas as pd
import uproot as ur

ANALYSIS_PMSSM_NAMES: Dict[str, str] = {
    "FullHad": "EwkFullHad2018",
    "1Lbb": "EwkOneLeptonTwoBjets2018",
    "Compressed": "EwkCompressed2018",
    "2L0J": "EwkTwoLeptonZeroJet2018",
    "2L2J": "EwkTwoLeptonTwoJet2018",
    "3LOffshell": "EwkThreeLeptonOffshell2018",
    "3LOnshellWZ": "EwkThreeLeptonOnshell2018",
    "3LOnshellWh": "EwkThreeLeptonOnshell2018",
    "4L": "FourLepton2018",
    # 'DT'	: 'DisappearingTrack",
    "S1L": "StopOneLepton2018",
    "SATACO": "SATACO"
}


def get_pMSSMFactory_params(all_columns: List[str] = []) -> Tuple[List[str], List[str]]:
    """
    Specify the parameters to get from the pMSSMFactory root files

    Args:
        all_columns (List[str], optional): List of all columns in root file. Defaults to [].

    Returns:
        Tuple[List[str], List[str]]: Tuple of lists of parameters and cross sections.
    """
    analyses: List[str] = [ANALYSIS_PMSSM_NAMES[analysis]
                           for analysis in ANALYSIS_PMSSM_NAMES]
    analysis_columns = ["EW_ExpCLs_Summary2018", "EW_ExpCLs_Summary",
                        "EW_ObsCLs_Summary2018", "EW_ObsCLs_Summary"]
    for c in all_columns:
        if "EWSmear" in c:
            continue
        if "_CatCLs_" in c:
            continue
        if "_ExpectedError_" in c:
            continue
        if "_ExpectedEvents_" in c:
            continue
        if "_err_" in c:
            continue
        if "_events_" in c and "_events_EwkTwoLeptonZeroJet2018__All" not in c:
            continue
        if "_acceptance_" in c:
            continue
        for a in analyses:
            if a in c:
                analysis_columns.append(c)

    # cross section columns
    xsec_columns: List[str] = [
        # see prospino_main.f90 for naming convention
        # nn = neutralino/chargino pair,
        # [1,2,3,4: neutralinos],[5,6: chi_1p, chi_2p],[7,8: chi_1m, chi_2m]
        "Cross_section_nn11", "Cross_section_nn12", "Cross_section_nn13",
        "Cross_section_nn14", "Cross_section_nn15", "Cross_section_nn16",
        "Cross_section_nn17", "Cross_section_nn18", "Cross_section_nn22",
        "Cross_section_nn23", "Cross_section_nn24", "Cross_section_nn25",
        "Cross_section_nn26", "Cross_section_nn27", "Cross_section_nn28",
        "Cross_section_nn33", "Cross_section_nn34", "Cross_section_nn35",
        "Cross_section_nn36", "Cross_section_nn37", "Cross_section_nn38",
        "Cross_section_nn44", "Cross_section_nn45", "Cross_section_nn46",
        "Cross_section_nn47", "Cross_section_nn48", "Cross_section_nn57",
        "Cross_section_nn58", "Cross_section_nn67", "Cross_section_nn68",
        "Cross_section_ll1", "Cross_section_ll2", "Cross_section_ll6",
        "Cross_section_ll7", "Cross_section_ll8", "Cross_section_ll16",
        "Cross_section_ll17",
        "Cross_section_EW",
    ]

    # branching fraction columns
    BF_columns: List[str] = [
        # 'BF_chi_1p_to_chi_10", "BF_chi_1p_to_chi_20",
        "BF_chi_20_to_chi_10_ele_ele", "BF_chi_20_to_chi_10_mu_mu", "BF_chi_20_to_chi_10_tau_tau",
        "BF_chi_20_to_chi_10_u_ubar", "BF_chi_20_to_chi_10_d_dbar", "BF_chi_20_to_chi_10_s_sbar",
        "BF_chi_20_to_chi_10_c_cbar", "BF_chi_20_to_chi_10_b_bbar", "BF_chi_20_to_chi_10_gam",
        "BF_chi_20_to_chi_1p_ele_nu_ele", "BF_chi_20_to_chi_1p_mu_nu_mu", "BF_chi_20_to_chi_1p_tau_nu_tau",
        "BF_chi_20_to_chi_10", "BF_chi_20_to_chi_10_Z",
        "BF_chi_20_to_chi_10_h", "BF_chi_20_to_chi_1p",
        "BF_chi_20_to_chi_1p_W", "BF_chi_20_to_chi_2p",
        "BF_chi_20_to_chi_2p_W", "BF_chi_2p_to_chi_10",
        "BF_chi_2p_to_chi_1p", "BF_chi_2p_to_chi_1p_Z",
        "BF_chi_2p_to_chi_1p_h", "BF_chi_2p_to_chi_20",
        "BF_h_to_b_b", "BF_H_to_b_b", "BF_h_to_chi_10_chi_10",
        "SI_BR_Bs_to_mumu", "SI_BR_Bu_to_taunu", "SI_BR_b_to_sgamma",
        # 'BF_chi_1p_to_chi_10_W",
        # 'BF_chi_1p_to_chi_10_e_L", "BF_chi_1p_to_chi_10_e_R", "BF_chi_1p_to_chi_10_mu_L", "BF_chi_1p_to_chi_10_mu_R",
        "w_chi_1p", "w_chi_20",
        "LLV_width",
    ]
    # SS_BF_columns = ['SS_'+ bf for bf in BF_columns]
    # BF_columns += SS_BF_columns

    # parameter columns
    param_columns: List[str] = ["model", "modelName", "value_m_h", "value_m_w_mssm", "value_m_w_sm",
                                "value_delta_rho", "BR_Bs_to_mumu", "BR_Bu_to_taunu", "BR_b_to_sgamma",  "Z_mssm_width",
                                "LSP_type", "m_chi_10", "m_chi_20",
                                "m_chi_30", "m_chi_40", "m_chi_1p", "m_chi_2p", "LSP_mass", "M_1", "M_2",
                                "M_3", "At", "Ab", "Atau", "mu", "tanb", "mA", "m_h", "m_H", "m_A",
                                "MO_Omega"]

    selected_columns = analysis_columns
    final_xsec_columns = []
    for c in xsec_columns:
        if c in all_columns:
            selected_columns.append(c)
            final_xsec_columns.append(c)
        else:
            print(f"WARNING: {c} not found in root file")
    for c in param_columns:
        if c in all_columns:
            selected_columns.append(c)
        else:
            print(f"WARNING: {c} not found in root file")
    for c in BF_columns:
        if c in all_columns:
            selected_columns.append(c)
        else:
            print(f"WARNING: {c} not found in root file")
    return (selected_columns, final_xsec_columns)


def __apply_LSP_cut(df: pd.DataFrame,
                    LSP_type: str = "") -> pd.DataFrame:
    """
Apply LSP cut to dataframe.

Args:
    df (pd.DataFrame): Dataframe to apply LSP cut to.
    LSP_type (str, optional): Select particular LSP type, eg. 'bino'/'wino'/'higgsino'/'neutralino'. Defaults to "".

Returns:
    pd.DataFrame: Dataframe with LSP cut applied.
"""
# valid LSP types
    LSP_dict: Dict[str, List[float]] = {
        "all": [],
        "bino": [1.0],
        "wino": [2.0],
        "higgsino": [3.0],
        "neutralino": [1.0, 2.0, 3.0],
        "EWK": [1.0, 3.0],
        "stau": [1000015.0],
        "sneutrino": [1000012.0, 1000014.0, 1000016.0],
        "tau_sneutrino": [1000016.0],
        "mu_sneutrino": [1000014.0],
        "ele_sneutrino": [1000012.0]}
    known_LSP_types: List[str] = [t for t in LSP_dict]

 # check LSP_type is valid
    if LSP_type not in known_LSP_types:
        raise ValueError(
            f"LSP_type {LSP_type} not recognised. Possible types are {known_LSP_types}")

    LSP_nums: List[float] = LSP_dict[LSP_type]
    if len(LSP_nums) == 0:
        return df
    cut = df["LSP_type"] == LSP_nums[0]
    for n in LSP_nums[1:]:
        cut = (cut) | (df["LSP_type"] == n)
    df = df[cut]
    return df


def apply_constraints(df: pd.DataFrame,
                      LSP_type: str = "",
                      verbose: bool = False) -> pd.DataFrame:
    """Apply contraints to dataframe.

    Args:
        df (pd.Dataframe): Dataframe to apply constraints to.
        LSP_type (str, optional): Select particular LSP type, eg. 'bino'/'wino'/'higgsino'/'neutralino'. Defaults to "".
        verbose (bool, optional): Print verbose output. Defaults to False.

    Returns:
        _type_: _description_
    """
    if LSP_type != "":
        n0 = len(df)
        df = __apply_LSP_cut(df, LSP_type)
        if verbose:
            print(len(df), "/", n0, "models have LSP type", LSP_type)
    return df


def __convert_root_results_to_df(path_root_file: str,
                                 columns: List[str] = [],
                                 rename_dict: Dict[str, str] = {},
                                 LSP_type: str = "") -> pd.DataFrame:
    """
    Open root files and return them as pandas dataframe

    Args:
        path_root_file (str): Path to root file.
        columns (List[str]): List of columns to extract. Defaults to [].
        rename_dict (Dict[str, str]): Dictionary to rename columns. Defaults to {}.
        program_cut (bool): Apply program cut. Defaults to False.
        LSP_type (str): Select particular LSP type, eg. "bino'/'wino'/'higgsino'/'neutralino'. Defaults to ''.
        DM_cut (bool): Apply DM constraint. Defaults to False.

    Returns:
        pd.DataFrame: Dataframe with results.
    """
    # suppress pandas performance warnings
    simplefilter(action="ignore", category=pd.errors.PerformanceWarning)

    # open root files and convert to pandas dataframe
    print(f"Processing file: {path_root_file}.")
    df_ = ur.open(path_root_file)["susy"]
    available_columns = df_.keys()
    grab_columns = []
    for c in columns:
        if c in available_columns:
            grab_columns.append(c)
        else:
            print(f"WARNING: {c} missing from {path_root_file}")

    # concatenate the df with the new chunk
    df = pd.DataFrame()
    try:
        for c in grab_columns:
            df[c] = df_[c].array(library="pd")
    except:
        print("WARNING: Could not extract all columns from", path_root_file)

    df = df.rename(columns=rename_dict)
    # set index to modelName
    df.index = df.modelName.values.astype(int)
    # apply contraints
    df = apply_constraints(df=df, LSP_type=LSP_type, verbose=True)
    df = df.sort_index(ascending=True)
    return df


def __convert_pMSSMFactory(path_root_file: str) -> pd.DataFrame:
    """
    Convert pMSSMFactory root files to pandas dataframe.

    Args:
        path_root_file (str): Path to root file.

    Returns:
        pd.DataFrame: Dataframe with results.
    """
    # peak ahead at columns in root files
    all_columns: List[str] = ur.open(path_root_file)["susy"].keys()

    # get parameter names
    param_columns, xsec_columns = get_pMSSMFactory_params(
        all_columns=all_columns)
    ewkino_dict: Dict[str, str] = {"1": "10", "2": "20", "3": "30", "4": "40", "5": "1p",
                                   "6": "2p", "7": "1m", "8": "2m"}
    ewkino_xsec_cols: List[str] = [n for n in xsec_columns if "nn" in n]
    slep_xsec_cols: List[str] = [n for n in xsec_columns if "ll" in n]
    rename_xsec: Dict[str, str] = {n: "xsec_"+ewkino_dict[n[-2]]+"_" +
                                   ewkino_dict[n[-1]] for n in ewkino_xsec_cols}
    rename_xsec["Cross_section_ll1"] = "xsec_seleLH"
    rename_xsec["Cross_section_ll2"] = "xsec_seleRH"
    rename_xsec["Cross_section_ll6"] = "xsec_stau11"
    rename_xsec["Cross_section_ll7"] = "xsec_stau22"
    rename_xsec["Cross_section_ll8"] = "xsec_stau12"
    rename_xsec["Cross_section_ll16"] = "xsec_smuLH"
    rename_xsec["Cross_section_ll17"] = "xsec_smuRH"
    rename_xsec["Cross_section_EW"] = "Cross_section_EW"
    # convert root files
    df: pd.DataFrame = __convert_root_results_to_df(
        path_root_file=path_root_file,
        columns=param_columns,
        rename_dict=rename_xsec)

    # check for duplicates
    duplicates: List[Any] = df[df.index.duplicated()].index.to_list()
    if len(duplicates) > 0:
        print("WARNING:", len(duplicates),
              "duplicated indices found - removing")
        df = df.drop_duplicates()
    rename_compressed = {}
    for c in df.columns:
        if "EwkCompressed2018_withBins" in c:
            rename_compressed[c] = c.replace("_withBins", "")
    df = df.rename(columns=rename_compressed)
    return df


def make_dataframe(path_root_file, final_csv_path=None):
    """
    Make a dataframe containing pMSSMFactory results, with various selections applied.

    Args:
         path_root_file (str): Path to root file of factory
    """
    df: pd.DataFrame = __convert_pMSSMFactory(
        path_root_file=path_root_file)
    if final_csv_path:
        df.to_csv(final_csv_path)
    return df


def df_setup(df, scanName=None):
    """
    Manipulations to apply to dataframes.

    Args: 
        df (pd.DataFrame): Dataframe to apply manipulations to.
        scanName (str): Name of scan.
        remove_failed_models (bool): Remove failed models. Defaults to True.

    Returns:
        pd.DataFrame: Dataframe with manipulations applied.
    """
    df["Scan"] = scanName
    # apply constraints
    df = apply_constraints(df, verbose=False)
    # fix dataframe index
    df.index = df.modelName.values.astype(int)
    df = df.sort_index(ascending=True)
    return df


def pick_best_CLs(df: pd.DataFrame,
                  subset: Literal["simplified", "LH",
                                  "Discovery", "All"] = "simplified",
                  decision_type: Literal["ExpCLs", "ObsCLs"] = "ExpCLs") -> None:
    """
    Pick the best expected CLs value and the corresponding observed CLs value from the df
    only picking a subset of analyses implemented in the pMSSMFactory.

    Args:
        df (pd.DataFrame): Dataframe from where the analyses are picked.
        subset (Literal["simplified", "LH", "Discovery", "All"]): Subset of available analyses types in the pMSSM factory.
        decision_type (Literal["ExpCLs", "ObsCLs"]): If "ExpCLs" is the decision type, the best expected CLs value is picked and the corresponding observed CLs value. 
            If "ObsCLs" is the decision type, the best observed CLs value is picked and the corresponding expected CLs value. Defaults to "ExpCLs".

    Returns:
        pd.DataFrame: Dataframe with new column that picked analyses as specified
    """
    # NOTE: this function can be optimized by vectorizing the operations
    df = df.copy()

    if decision_type == "ExpCLs":
        anti_decision_type: str = "ObsCLs"
    elif decision_type == "ObsCLs":
        anti_decision_type: str = "ExpCLs"

    # names of the new columns to be added to the df
    new_names: Tuple[str, str] = (
        f"SATACO_Comparison_BestExpCLs_{subset}", f"SATACO_Comparison_BestObsCLs_{subset}")

    if new_names[0] in df.columns and new_names[1] in df.columns:
        # remove the columns if they already exist
        df = df.drop(columns=[new_names[0], new_names[1]])

    columns_oi: List[str] = []
    if subset == "simplified":
        # get all columns that contain the substring "simplified"
        columns_oi = [
            col for col in df.columns if "simplified" in col and decision_type in col]

    elif subset == "All":
        columns_oi = [
            col for col in df.columns if decision_type in col]

    elif subset == "LH" or subset == "Discovery":
        NotImplementedError("Not implemented yet")

    # in each row, pick the best CLs value from the columns_oi
    df_: pd.DataFrame = pd.DataFrame(columns=new_names)
    df_dict: Dict[str, List[float]] = {new_names[0]: [], new_names[1]: []}
    if decision_type == "ExpCLs":
        # get the best expected CLs value and the corresponding observed CLs value
        for idx, row in df.iterrows():
            # get the best expected CLs value and the corresponding observed CLs value
            best_expCLs: float = min(row[columns_oi])
            min_arg: str = row[columns_oi].idxmin()
            print(min_arg)
            min_arg = min_arg.replace(decision_type, anti_decision_type)
            best_obsCLs: float = row[min_arg]
            # add the values to the  dict
            df_dict[new_names[0]].append(best_expCLs)
            df_dict[new_names[1]].append(best_obsCLs)

    elif decision_type == "ObsCLs":
        # get the best observed CLs value and the corresponding expected CLs value
        for idx, row in df.iterrows():
            # get the best observed CLs value and the corresponding expected CLs value
            best_obsCLs: float = min(row[columns_oi])
            min_arg: str = row[columns_oi].idxmin()
            min_arg = min_arg.replace(decision_type, anti_decision_type)
            best_expCLs: float = row[min_arg]
            # add the values to the  dict
            df_dict[new_names[0]].append(best_expCLs)
            df_dict[new_names[1]].append(best_obsCLs)

    # add the new columns to the df
    df_ = pd.DataFrame(df_dict)
    df = pd.concat([df, df_], axis=1)

    return df


if __name__ == "__main__":
    # path to the root file with the results
    path_root_file: str = os.path.join(os.path.dirname(__file__),
                                       "..",
                                       "..",
                                       "tests_data",
                                       "tests_data_processing_tools",
                                       "susy-LeonBinopMSSMRun2.root")

    df = make_dataframe(path_root_file=path_root_file,
                        final_csv_path=None)
    # print("SUCCESS: dataframe created")

    # setup dataframe
    df = df_setup(df, scanName="EWK")

    # save as final_setup.csv
    df.to_csv(os.path.join(os.path.dirname(__file__), "final_setup.csv"))
    df = pd.read_csv(os.path.join(
        os.path.dirname(__file__), "final_setup.csv"))

    print("SUCCESS: dataframe setup")

    # pick best CLs values
    df = pick_best_CLs(df=df, subset="simplified", decision_type="ExpCLs")
    print(df[["SATACO_Comparison_BestObsCLs_simplified",
          "SATACO_Comparison_BestExpCLs_simplified"]])

    df.to_csv(os.path.join(os.path.dirname(__file__), "final_setup.csv"))
