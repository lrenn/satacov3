"""
Test the function propose_combinations.
"""

import os
import sys
from typing import Any, Dict, List

import numpy as np
import pandas as pd
import pytest
from skopt.space import Real

from SATACO.learning_tools.actively_learn_overlaps import (
    __negative_signal_efficiency_function, all_pMSSM_parameters,
    learn_pMSSM_points, merge_efficiencies_2_parameters,
    restrict_parameter_length, retrieve_dimension_borders)

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_merge_efficiencies_2_parameters():
    """
    Test the function that merges the dataframes and calculates the 
    signal efficiencies with the correspnding functions
    """
    # Sample data for testing
    efficiencies_data: Dict[str, List[Any]] = {
        "Model": [1, 2, 3],
        "SR1": [4, 5, 7],
        "SR2": [10, 10, 10],
        "n": [100000, 95000, 90000]
    }

    parameters_data: Dict[str, List[Any]] = {
        "Model": [1, 2, 3],
        "M_1": [20.0, 21.0, 22.0],
        "tanb": [-1, -2, -3]
    }

    efficiencies_df = pd.DataFrame(efficiencies_data)
    parameters_df = pd.DataFrame(parameters_data)
    combination = "SR1&SR2"
    result_df = merge_efficiencies_2_parameters(
        efficiencies_df,
        parameters_df,
        combination)

    # Check if the result has the expected number of rows
    assert len(result_df) == 3

    # Check if the result df has the right columns
    assert sorted(list(result_df.columns)) == sorted(
        ["Model", "M_1", "tanb", "SR1", "SR2", "n", "neg_eff"])

    # Check if missing models are kicked out (1. when missing in params)
    parameters_data: Dict[str, List[Any]] = {
        "Model": [1, 2],
        "M_1": [20.0, 21.0],
        "tanb": [-1, -2]
    }
    parameters_df = pd.DataFrame(parameters_data)
    result_df = merge_efficiencies_2_parameters(
        efficiencies_df,
        parameters_df,
        combination)
    assert len(result_df) == 2

    # Test for invalid combination
    combination = "InvalidCombination"
    with pytest.raises(ValueError):
        merge_efficiencies_2_parameters(
            efficiencies_df, parameters_df, combination)

    # Test for invalid keys
    combination = "SR1&SR3"
    with pytest.raises(KeyError):
        merge_efficiencies_2_parameters(
            efficiencies_df, parameters_df, combination)


def test_negative_signal_efficiency_function():
    """
    Test the function that will be used for the minimization process
    by the Active Learning process.
    """
    alpha: float = 0.9
    yields_i: np.ndarray = np.array([10, 5])
    yields_j: np.ndarray = np.array([10, 10])
    n: np.ndarray = np.array([100, 100])
    assert np.allclose(__negative_signal_efficiency_function(
        yields_i=yields_i,
        yields_j=yields_j,
        n_total=n,
        signal_eff_alpha=alpha),
        np.array([-0.119, -0.113]))


def test_restrict_parameter_length():
    """
    Function to test the restrict parameter length function.
    """
    pMSSM_parameters: List[str] = [
        'tanb',
        'M_1',
        'M_2',
        'M_3',
        'At',
        'Ab',
        'Atau',
        'mu',
        'mA',
        'meL',
        'mmuL',
        'mtauL',
        'meR',
        'mmuR',
        'mtauR',
        'mqL1',
        'mqL2',
        'mqL3',
        'muR',
        'mcR',
        'mtR',
        'mdR',
        'msR',
        'mbR'
    ]

    # test if the function returns the correct parameters
    true_restricted_parameters: List[str] = [
        'tanb',
        'M_1',
        'M_2',
        'M_3',
        'At',
        'Ab',
        'Atau',
        'mu',
        'mA',
        'meL',
        'mtauL',
        'meR',
        'mtauR',
        'mqL1',
        'mqL3',
        'muR',
        'mtR',
        'mdR',
        'mbR'
    ]
    assert sorted(restrict_parameter_length(
        pMSSM_parameters)) == sorted(true_restricted_parameters)

    # If less parameters are given, the function should return a list
    # of the same length but with the standard parameters
    assert sorted(restrict_parameter_length(['tanb', 'M_1', 'mmuL'])) == sorted([
        'tanb', 'M_1', 'meL'])


def test_retrieve_dimension_borders():
    """
    Test the function to get the dimensions from the dataframe.
    """
    # pMSSM params that one should get the borders
    pMSSM_parameters: List[str] = ["M_1",
                                   "tanb_ext"
                                   ]
    # read with pandas
    param_df: pd.DataFrame = pd.DataFrame(data=[[100., 200.], [40., 40.], [20., 30.]],
                                          columns=pMSSM_parameters)
    print(param_df)

    # true borders
    true_borders: Dict[str, Real] = {
        "M_1": Real(20., 100., prior="uniform", transform="identity"),
        "tanb_ext": Real(30., 200., prior="uniform", transform="identity")}

    # read borders from csv
    borders: Dict[str, Real] = retrieve_dimension_borders(parameter_dataframe=param_df,
                                                          pMSSM_parameters=pMSSM_parameters,
                                                          smear_borders=0.
                                                          )

    assert true_borders == borders

    # try with different smearing factor
    borders = retrieve_dimension_borders(parameter_dataframe=param_df,
                                         pMSSM_parameters=pMSSM_parameters,
                                         smear_borders=0.1
                                         )

    # true borders
    true_borders: Dict[str, Real] = {
        "M_1": Real(18., 110., prior="uniform", transform="identity"),
        "tanb_ext": Real(27., 220., prior="uniform", transform="identity")}

    # test with wrog keyparameter
    pMSSM_parameters.append(["not_valid"])

    with pytest.raises(KeyError):
        borders = retrieve_dimension_borders(parameter_dataframe=param_df,
                                             pMSSM_parameters=pMSSM_parameters,
                                             smear_borders=0.
                                             )


def test_all_pMSSM_parameters():
    """
    Test the all parameters function.
    """
    # the true pMSSM parameters
    pMSSM_parameters: List[str] = [
        'tanb',
        'M_1',
        'M_2',
        'M_3',
        'At',
        'Ab',
        'Atau',
        'mu',
        'mA',
        'meL',
        'mmuL',
        'mtauL',
        'meR',
        'mmuR',
        'mtauR',
        'mqL1',
        'mqL2',
        'mqL3',
        'muR',
        'mcR',
        'mtR',
        'mdR',
        'msR',
        'mbR'
    ]

    # compare all elements by using all
    assert pMSSM_parameters == all_pMSSM_parameters()


def test_learn_pMSSM_point():
    """
    Test the function learn_pMSSM_point.
    """
    # Data for testing
    df_pmssm_eff_data: Dict[str, List[Any]] = {
        "Model": [1, 2, 3],
        "M_1": [20.0, 21.0, 22.0],
        "tanb": [-1, -2, -3],
        "SR1": [4, 5, 7],
        "SR2": [10, 10, 10],
        "n": [100000, 95000, 90000],
        "neg_eff": [-0.119, -0.113, -0.1]
    }
    # Data for dimension borders
    dimension_borders: Dict[str, Real] = {
        "M_1": Real(19., 23., prior="uniform", transform="identity"),
        "tanb": Real(-4., 0., prior="uniform", transform="identity")
    }
    # Run the function
    result_df: pd.DataFrame = learn_pMSSM_points(df_pmssm_eff=pd.DataFrame(df_pmssm_eff_data),
                                                 dimension_borders=dimension_borders,
                                                 num_next_points=2)

    # Check if the result has the expected number of rows
    assert len(result_df) == 2

    # Check if the result df has the right columns
    assert sorted(list(result_df.columns)) == sorted(
        ["tanb", "M_1"])
