"""
Testing the functions to transform the paths to readable format.
"""


import os
import sys

from SATACO.graph_tools.transform_to_readable import \
    __indices_to_sr_names as indices_to_sr_names

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_indices_to_sr_names():
    """Test indices_to_sr_names."""
    sr_names = ["SR1", "SR2", "SR3", "SR4", "SR5", "SR6", "SR7"]

    result_dict = {
        0: {
            "path": [0, 1, 2, 3],
            "weight": 1.0
        },
        1: {
            "path": [0, 1, 2, 3, 4, 5, 6],
            "weight": 1.0
        }
    }

    expected_result = {
        0: {
            "path": ["SR1", "SR2", "SR3", "SR4"],
            "weight": 1.0
        },
        1: {
            "path": ["SR1", "SR2", "SR3", "SR4", "SR5", "SR6", "SR7"],
            "weight": 1.0
        }
    }

    result = indices_to_sr_names(sr_names, result_dict)

    assert result == expected_result
