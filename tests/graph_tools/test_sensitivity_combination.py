"""
Testing the sensitivity combination functions.
"""

import os
import sys

import numpy as np

from SATACO.graph_tools.sensitivity_combination import (stouffers_method,
                                                        summation)

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_summation():
    """
    Testing the summation function.
    """
    # Test case 1: Empty path and weight
    assert summation([], np.array([])) == 0

    # Test case 2: Non-empty path and weight
    path = [0, 1, 2]
    weight = np.array([0.1, 0.2, 0.3])
    assert np.isclose(summation(path, weight), 0.6)


def test_stouffers_method():
    """
    Testing the stouffers_method function.
    """
    # Test case 1: Empty path and weight
    assert stouffers_method([], np.array([])) == 0

    # Test case 2: Non-empty path and weight
    path = [0, 1, 2]
    weight = np.array([0.1, 0.2, 0.3])
    assert np.isclose(stouffers_method(path, weight), 0.6 / np.sqrt(3))
