"""
Test for plotting different matrices.
"""

import os
import sys

import numpy as np
import pytest

from SATACO.plotting_tools.plot_matrices import (plot_events_matrices,
                                                 plot_matrix_differences,
                                                 plot_overlap_matrix,
                                                 plot_statistics_matrices,
                                                 plot_UL_OVER_scatter)

DIRNAME = os.path.dirname(__file__)

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_plot_overlap_matrix():
    """
    Test the plot_overlap_matrix function.
    """
    # Create a random matrix
    matrix = np.random.uniform(0, 1, (10, 10))

    # names of the columns
    column_names = []
    column_names = [
        "Analysis1__LH0__SR0",
        "Analysis1__LH0__SR1",
        "Analysis2__LH0__SR0",
        "Analysis3__LH0__SR0",
        "Analysis3__LH1__SR0",
        "Analysis3__LH1__SR1",
        "Analysis4__LH0__SR0",
        "Analysis5__LH0__SR0",
        "Analysis5__LH0__SR1",
        "Analysis5__LH0__SR2",
    ]

    # provide a production mode group
    production_mode_group = {"Analysis0": "EW",
                             "Analysis1": "EW",
                             "Analysis2": "EW",
                             "Analysis3": "EW",
                             "Analysis4": "Other",
                             "Analysis5": "Strong"}

    # Test the function
    matrix[0, 0] = np.nan
    plot_overlap_matrix(matrix=matrix,
                        column_names=column_names,
                        production_mode_grouping=production_mode_group,
                        mark_likelihood_scans=True,
                        mark_nan_values=True,
                        save=DIRNAME + "/test_plot_overlap_matrix.png")

    # test if file is created in this directory
    assert os.path.isfile(DIRNAME +
                          "/test_plot_overlap_matrix.png")

    # remove the file
    os.remove(DIRNAME +
              "/test_plot_overlap_matrix.png")


@pytest.mark.filterwarnings("ignore::UserWarning")
def test_plot_events_matrices():
    """
    Test the plot_events_matrices function.
    """
    # create random matrices
    shared_events = np.random.randint(0, 100, (10, 10))

    # names of the columns
    column_names = []
    for i in range(shared_events.shape[1]):
        column_names.append("Analysis {}".format(i))

    # Test the function
    plot_events_matrices(shared_events=shared_events,
                         column_names=column_names,
                         save=DIRNAME + "/test_plot_events_matrices.png")

    # test if file is created in this directory
    assert os.path.isfile(DIRNAME +
                          "/test_plot_events_matrices.png")

    # remove the file
    os.remove(DIRNAME +
              "/test_plot_events_matrices.png")


@pytest.mark.filterwarnings("ignore::UserWarning")
def test_plot_statistics_matrices():
    """
    Test the plot_statistics_matrices function.
    """
    # create random matrices
    significant_correlations = np.random.randint(-1, 1, (10, 10))
    upper_limits = np.random.uniform(0, 1, (10, 10))

    # names of the columns
    column_names = []
    for i in range(significant_correlations.shape[1]):
        column_names.append("Analysis {}".format(i))

    # Test the function
    plot_statistics_matrices(significant_correlations=significant_correlations,
                             upper_limits=upper_limits,
                             column_names=column_names,
                             save=DIRNAME + "/test_plot_statistics_matrices.png")

    # test if file is created in this directory
    assert os.path.isfile(DIRNAME +
                          "/test_plot_statistics_matrices.png")

    # remove the file
    os.remove(DIRNAME +
              "/test_plot_statistics_matrices.png")

    # reshape significant_correlations and test again
    significant_correlations = np.random.randint(-1, 1, (10, 10, 1))

    # test the function
    with pytest.raises(ValueError):
        plot_statistics_matrices(significant_correlations=significant_correlations,
                                 upper_limits=upper_limits,
                                 column_names=column_names,
                                 save=os.path.dirname(
                                     __file__) + "/test_plot_statistics_matrices.png")


@pytest.mark.filterwarnings("ignore::UserWarning")
def test_plot_matrix_differences():
    """
    Test the plot_matrix_differences function.
    """
    np.random.seed(42)
    # create random matrices
    matrix_A = np.random.uniform(0, 1, (10, 10))
    matrix_B = np.random.uniform(0, 1, (10, 10))

    # make them symmetric
    matrix_A = matrix_A + matrix_A.T
    matrix_B = matrix_B + matrix_B.T

    # make the diagonal elements one
    np.fill_diagonal(matrix_A, 1)
    np.fill_diagonal(matrix_B, 1)

    # names of the columns
    column_names = []
    for i in range(matrix_A.shape[1]):
        column_names.append("Analysis {}".format(i))

    # Test the function
    plot_matrix_differences(matrix_A=matrix_A,
                            matrix_B=matrix_B,
                            column_names=column_names,
                            save=DIRNAME + "/test_plot_matrix_differences.png")

    # test if file is created in this directory
    assert os.path.isfile(DIRNAME +
                          "/test_plot_matrix_differences.png")

    with pytest.raises(ValueError):
        plot_matrix_differences(matrix_A=matrix_A,
                                matrix_B=matrix_B.reshape(10, 10, 1),
                                column_names=column_names,
                                save=DIRNAME + "/test_plot_matrix_differences.png")

    # remove the file
    os.remove(DIRNAME +
              "/test_plot_matrix_differences.png")


@pytest.mark.filterwarnings("ignore::UserWarning")
def test_plot_UL_OVER_scatter():
    """
    Test the plot_UL_OVER_scatter function.
    """
    upper_limits = np.random.uniform(0, 1, (10, 10))
    overlaps = np.random.uniform(0, 1, (10, 10))

    plot_UL_OVER_scatter(upper_limits_matrix=upper_limits,
                         overlaps_matrix=overlaps,
                         save=DIRNAME + "/test_plot_UL_OVER_scatter.png")

    # test if file is created in this directory
    assert os.path.isfile(DIRNAME +
                          "/test_plot_UL_OVER_scatter.png")

    with pytest.raises(ValueError):
        plot_UL_OVER_scatter(upper_limits_matrix=upper_limits.reshape(10, 10, 1),
                             overlaps_matrix=overlaps,
                             save=DIRNAME + "/test_plot_UL_OVER_scatter.png")

    # remove the file
    os.remove(DIRNAME +
              "/test_plot_UL_OVER_scatter.png")
