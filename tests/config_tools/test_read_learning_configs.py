"""
Testing the read_sataco_configs.py function.
"""

import os
import sys

import pytest

from SATACO.config_tools.read_learning_configs import read_learning_configs

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_read_learning_configs():
    # specify the path of the test configuration files
    test_valid_config_file = os.path.join(
        os.path.dirname(__file__),
        '..',
        '..',
        'tests_data',
        'tests_data_config_tools',
        'test_learning_valid.config')
    test_missing_option_config_file = os.path.join(
        os.path.dirname(__file__),
        '..',
        '..',
        'tests_data',
        'tests_data_config_tools',
        'test_learning_missing_option.config')
    test_missing_section_config_file = os.path.join(
        os.path.dirname(__file__),
        '..',
        '..',
        'tests_data',
        'tests_data_config_tools',
        'test_learning_missing_section.config')
    test_value_error_config_file = os.path.join(
        os.path.dirname(__file__),
        '..',
        '..',
        'tests_data',
        'tests_data_config_tools',
        'test_learning_value_error.config')
    test_empty_config_file = os.path.join(
        os.path.dirname(__file__),
        '..',
        '..',
        'tests_data',
        'tests_data_config_tools',
        'test_learning_empty.config')

    # test valid config file
    config_values = read_learning_configs(test_valid_config_file)
    true_config_values = {
        'acquisition_function': 'EI',
        'minimization_function': 'GP',
        'acquisition_optimizer': 'LBFGS',
        'pmssm_parameters': 'ALL',
        'num_next_points': 5,
        'signal_eff_alpha': 0.5}

    assert config_values == true_config_values

    # test missing option config file
    with pytest.raises(SystemExit):
        config_values = read_learning_configs(test_missing_option_config_file)

    # test missing section config file
    with pytest.raises(SystemExit):
        config_values = read_learning_configs(test_missing_section_config_file)

    # test value error config file
    with pytest.raises(SystemExit):
        config_values = read_learning_configs(test_value_error_config_file)

    # test non-existent config file
    with pytest.raises(SystemExit):
        config_values = read_learning_configs('non-existent.config')

    # test empty config file
    with pytest.raises(SystemExit):
        config_values = read_learning_configs(test_empty_config_file)

    return
