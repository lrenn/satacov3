"""
Test the functions that check the type and
value of the config file arguments.
"""

import os
import sys

from SATACO.config_tools.check_sataco_configs import \
    __check_combination_method as check_combination_method
from SATACO.config_tools.check_sataco_configs import \
    __check_confidence_level as check_confidence_level
from SATACO.config_tools.check_sataco_configs import \
    __check_correlation_method as check_correlation_method
from SATACO.config_tools.check_sataco_configs import \
    __check_correlation_threshold as check_correlation_threshold
from SATACO.config_tools.check_sataco_configs import \
    __check_weight_method as check_weight_method

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_check_weight_method():
    """
    Test the check_weight_method function.
    """
    # test if the function returns True for a 'unit'
    assert check_weight_method('unit') == True

    # test if the function returns True for a 'gaussian_approx_wo_error'
    assert check_weight_method('gaussian_approx_wo_error') == True

    # test if the function returns True for a 'gaussian_approx_w_error'
    assert check_weight_method('gaussian_approx_w_error') == True

    # test if the function returns True for a
    # 'recommended_sensitivity_estimation'
    assert check_weight_method('recommended_sensitivity_estimation') == True

    # test if the function returns True for a
    # 'recommended_sensitivity_estimation'
    assert check_weight_method(
        'recommended_sensitivity_estimation_wo_error') == True

    # test if the function returns False for a wrong type
    assert check_weight_method(1) == False

    # test if the function returns False for a wrong value
    assert check_weight_method('not_valid') == False


def test_check_correlation_method():
    """
    Test the check_correlation_method function.
    """
    # test if the function returns True for a 'pearson'
    assert check_correlation_method('szymkiewicz_simpson') == True

    # test if the function returns True for a 'geometrical'
    assert check_correlation_method('geometrical') == True

    # test if the function returns False for a wrong type
    assert check_correlation_method(1) == False

    # test if the function returns False for a wrong value
    assert check_correlation_method('not_valid') == False


def test_check_confidence_level():
    """
    Test the check_confidence_level function.
    """
    # test if the function returns True for a 0.95
    assert check_confidence_level(0.95) == True

    # test if the function returns True for a 0.99
    assert check_confidence_level(0.99) == True

    # test if the function returns False for -0.01
    assert check_confidence_level(-0.01) == False

    # test if the function returns False for 1.01
    assert check_confidence_level(1.01) == False

    # test if the function returns False for a wrong type
    assert check_confidence_level('not_valid') == False


def test_check_correlation_threshold():
    """
    Test the check_correlation_threshold function.
    """
    # test if the function returns True for a 0.95
    assert check_correlation_threshold(0.95) == True

    # test if the function returns True for a 0.99
    assert check_correlation_threshold(0.99) == True

    # test if the function returns False for -0.01
    assert check_correlation_threshold(-0.01) == False

    # test if the function returns False for 1.01
    assert check_correlation_threshold(1.01) == False

    # test if the function returns False for a wrong type
    assert check_correlation_threshold('not_valid') == False


def test_check_combination_method():
    """
    Test the check_combination_method function.
    """
    # test if the function returns True for 'stouffer'
    assert check_combination_method('stouffer') == True

    # test if the function returns True for 'adding'
    assert check_combination_method('adding') == True

    # test if the function returns False for a wrong type
    assert check_combination_method(1) == False

    # test if the function returns False for a wrong value
    assert check_combination_method('not_valid') == False
