"""
Test sorting functions for matrices.
"""

import os
import sys

import numpy as np
import pytest

from SATACO.processing_tools.process_sorting import sort_matrix_column_names

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_sort_matrix_column_names():
    """
    Test sorting of a matrix.
    """
    # define the symmetric matrix
    matrix = np.array([[1., 0.1, 0.2],
                       [0.1, 1., 0.3],
                       [0.2, 0.3, 1.]])

    # define the original column names
    column_names_original = ["A", "B", "C"]

    # define the new column names
    column_names_new = ["B", "C", "A"]

    # define the sorted matrix
    matrix_sorted = np.array([[1., 0.3, 0.1],
                              [0.3, 1., 0.2],
                              [0.1, 0.2, 1.]])

    # sort the matrix
    matrix_sorted_test = sort_matrix_column_names(matrix=matrix,
                                                  column_names_original=column_names_original,
                                                  column_names_new=column_names_new)

    # check that the matrix is sorted
    assert np.allclose(matrix_sorted, matrix_sorted_test, atol=1e-4)

    # check that the sorting algorithm does not work if the column names are not unique
    column_names_new = ["B", "C", "B"]
    with pytest.raises(ValueError):
        sort_matrix_column_names(matrix=matrix,
                                 column_names_original=column_names_original,
                                 column_names_new=column_names_new)

    # check that the sorting algorithm does not work if the column names are not the same
    column_names_new = ["B", "C", "D"]
    with pytest.raises(ValueError):
        sort_matrix_column_names(matrix=matrix,
                                 column_names_original=column_names_original,
                                 column_names_new=column_names_new)

    # check that the sorting algorithm does not work if the matrix has a different number of columns than the column names
    column_names_new = ["B", "C", "A", "D"]
    with pytest.raises(ValueError):
        sort_matrix_column_names(matrix=matrix,
                                 column_names_original=column_names_original,
                                 column_names_new=column_names_new)

    # check that the sorting algorithm does not work if the matrix has a different number of columns than the column names
    column_names_new = ["B", "C"]
    with pytest.raises(ValueError):
        sort_matrix_column_names(matrix=matrix,
                                 column_names_original=column_names_original,
                                 column_names_new=column_names_new)
