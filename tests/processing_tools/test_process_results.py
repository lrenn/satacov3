"""
Test the process results module.
"""


import os
import sys

from SATACO.processing_tools.process_results import df_setup, make_dataframe

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

if __name__ == "__main__":

    # path to the root file with the results
    path_root_file: str = os.path.join(os.path.dirname(__file__),
                                       "..",
                                       "..",
                                       "tests_data",
                                       "tests_data_processing_tools.root")

    df = make_dataframe(path_root_file, os.path.dirname(__file__), "final.csv")
    print("SUCCESS: dataframe created")
