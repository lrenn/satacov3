matplotlib>=3.5.3
numpy>=1.21.6
pandas>=1.3.5
networkx>=2.6
scipy>=1.7.0
pytest>=7.2.2
more-itertools>=9.0.0
scikit-optimize>=0.9.0
mplhep>=0.3.31
uproot>=5.1.2
